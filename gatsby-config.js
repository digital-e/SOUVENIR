require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})

module.exports = {
    pathPrefix: "/SOUVENIR",
  siteMetadata: {
    title: `SOUVENIR OFFICIAL`,
    description: `SOUVENIR OFFICIAL ONLINE STORE`,
    author: `Samuel Bassett <samuelbassett.xyz>`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: "gatsby-source-prismic",
      options: {
        // The name of your prismic.io repository. This is required.
        // Example: 'gatsby-source-prismic-test-site' if your prismic.io address
        // is 'gatsby-source-prismic-test-site.prismic.io'.
        repositoryName: "s0uvenirofficial",

        // An API access token to your prismic.io repository. This is required.
        // You can generate an access token in the "API & Security" section of
        // your repository settings. Setting a "Callback URL" is not necessary.
        // The token will be listed under "Permanent access tokens".
        accessToken:
          "MC5YWnkzLVJFQUFDUUFBVGox.I1Tvv712GSM_77-977-977-977-977-977-9BkLvv73vv71D77-9GXgw77-9VGDvv71jFg4qdu-_vQ",
        schemas: {
          about: require("./src/schemas/about.json"),
          info: require("./src/schemas/info.json"),
          announcement_banner: require("./src/schemas/announcement_banner.json"),
          berlin_store: require("./src/schemas/berlin_store.json"),
          contact: require("./src/schemas/contact.json"),
          frequently_asked_questions: require("./src/schemas/frequently_asked_questions.json"),
          imprint: require("./src/schemas/imprint.json"),
          privacy_policy: require("./src/schemas/privacy_policy.json"),
          product_shipping_information: require("./src/schemas/product_shipping_information.json"),
          shipping_and_returns: require("./src/schemas/shipping_and_returns.json"),
          standard_banner: require("./src/schemas/standard_banner.json"),
          story: require("./src/schemas/story.json"),
          terms_of_use: require("./src/schemas/terms_of_use.json"),
          landing_gallery: require("./src/schemas/landing_gallery.json"),
        },

        shouldDownloadImage: () => true,

        // Set a link resolver function used to process links in your content.
        // Fields with rich text formatting or links to internal content use this
        // function to generate the correct link URL.
        // The document node, field key (i.e. API ID), and field value are
        // provided to the function, as seen below. This allows you to use
        // different link resolver logic for each field if necessary.
        // See: https://prismic.io/docs/javascript/query-the-api/link-resolving
        linkResolver: ({ node, key, value }) => doc => {
          // Your link resolver
        },

        // Set a list of links to fetch and be made available in your link
        // resolver function.
        // See: https://prismic.io/docs/javascript/query-the-api/fetch-linked-document-fields
        fetchLinks: [
          // Your list of links
        ],

        // Set an HTML serializer function used to process formatted content.
        // Fields with rich text formatting use this function to generate the
        // correct HTML.
        // The document node, field key (i.e. API ID), and field value are
        // provided to the function, as seen below. This allows you to use
        // different HTML serializer logic for each field if necessary.
        // See: https://prismic.io/docs/nodejs/beyond-the-api/html-serializer
        htmlSerializer: ({ node, key, value }) => (
          type,
          element,
          content,
          children
        ) => {
          // Your HTML serializer
        },
        lang: "*",
        // shouldNormalizeImage: ({ node, key, value }) => {
        //   // Return true to normalize the image or false to skip.
        // },
      },
    },
    {
      resolve: "gatsby-source-shopify",
      options: {
        // The domain name of your Shopify shop. This is required.
        // Example: 'gatsby-source-shopify-test-shop' if your Shopify address is
        // 'gatsby-source-shopify-test-shop.myshopify.com'.
        // shopName: "graphql",
        shopName: process.env.SHOP_NAME,

        // An API access token to your Shopify shop. This is required.
        // You can generate an access token in the "Manage private apps" section
        // of your shop's Apps settings. In the Storefront API section, be sure
        // to select "Allow this app to access your storefront data using the
        // Storefront API".
        // See: https://help.shopify.com/api/custom-storefronts/storefront-api/getting-started#authentication
        accessToken: process.env.SHOPIFY_ACCESS_TOKEN,

        // Set verbose to true to display a verbose output on `npm run develop`
        // or `npm run build`. This prints which nodes are being fetched and how
        // much time was required to fetch and process the data.
        // Defaults to true.
        verbose: true,

        // Number of records to fetch on each request when building the cache
        // at startup. If your application encounters timeout errors during
        // startup, try decreasing this number.
        paginationSize: 250,

        // List of collections you want to fetch.
        // Possible values are: 'shop' and 'content'.
        // Defaults to ['shop', 'content'].
        includeCollections: ["shop", "content"],
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-styled-components`,
      options: {
        // Add any options here
      },
    },
    {
      resolve: `gatsby-plugin-create-client-paths`,
      options: { prefixes: [`/unpublishedPreview/*`] },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/favicon.png`, // This path is relative to the root of the site.
      },
    },
  ],
}
