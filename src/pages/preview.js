import React, { useEffect } from "react"
import { navigate } from "gatsby"
import { usePrismicPreview } from "gatsby-source-prismic"

const PreviewPage = ({ location, data }) => {
  const { allPrismicStory } = data

  const pageUIDs = allPrismicStory.nodes.map(node => node.uid)

  //Add Info Page
  pageUIDs.push("info")

  const pathResolver = () => doc => {
    const previewedUID = doc.uid

    if (pageUIDs.includes(previewedUID)) {
      return previewedUID
    } else {
      return "/unpublishedPreview"
    }
  }

  const { previewData, path } = usePrismicPreview(location, {
    repositoryName: "s0uvenirofficial",
    pathResolver,
  })

  //   if (data.previewData !== null) {
  //     window.__PRISMIC_PREVIEW_DATA__ = data.previewData
  //     navigate(Object.values(data.previewData)[0].uid)
  //   }
  // }, [data])

  useEffect(() => {
    if (previewData && path) {
      window.__PRISMIC_PREVIEW_DATA = previewData
      navigate(path)
    }
  }, [path, previewData])

  return <div>Loading preview...</div>
}

export default PreviewPage

export const query = graphql`
  {
    allPrismicStory {
      nodes {
        uid
      }
    }
  }
`
