import React, { useEffect } from "react"

import styled from "styled-components"

import StoriesGrid from "../components/storiesgrid"

import Footer from "../components/footer"

import SEO from "../components/seo"

const Spacer = styled.div`
  width: 100%;
  height: 5.5rem;

  @media (min-width: 576px) {
    height: 8.5rem;
  }
`

const StoriesPage = props => {
  useEffect(() => {
    setTimeout(() => {
      window.scrollTo(0, 0)
    }, 0)
  })
  return (
    <>
      <SEO title="STORIES" />
      <Spacer />
      <StoriesGrid />
      <Footer />
    </>
  )
}

export default StoriesPage
