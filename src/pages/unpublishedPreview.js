import React from "react"
import StoryPage from "../templates/storypage.js"

const IS_BROWSER = typeof window !== "undefined"

const UnpublishedPage = props => {
  let previewData = null
  if (IS_BROWSER && window.__PRISMIC_PREVIEW_DATA) {
    previewData = window.__PRISMIC_PREVIEW_DATA.prismicStory.data
  }

  // => Perform any logic from previewData to determine the correct page or template component to use.
  {
    return previewData !== null ? <StoryPage {...props} /> : null
  }
}

export default UnpublishedPage
