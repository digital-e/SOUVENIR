import React from "react"
import { Link, graphql } from "gatsby"

import SEO from "../components/seo"

import ProductsGrid from "../components/productsgrid"
import LandingGallery from "../components/landing-gallery"

import CookieBanner from "../components/cookie-banner"
import SouvenirTicker from "../components/souvenir-ticker"

import Footer from "../components/footer"

const IndexPage = props => {
  return (
    <>
      <SEO title="SOUVENIR OFFICIAL" />

      <LandingGallery
        data={props.data.prismicLandingGallery}
        products={props.data.allShopifyCollection.edges[0].node.products}
      />
      <ProductsGrid
        allProducts={props.data.allShopifyCollection.edges[0].node.products}
        isLanding="true"
      />
      <SouvenirTicker fixed={true} />
      <CookieBanner />
      <Footer />
    </>
  )
}

export default IndexPage

// export const query = graphql`
//   query {
//     prismicSlideshow {
//       id
//       data {
//         gallery {
//           image {
//             url
//             fluid(maxWidth: 1000) {
//               ...GatsbyPrismicImageFluid
//             }
//           }
//         }
//         interval_time
//       }
//     }
//   }
// `

export const query = graphql`
  {
    prismicLandingGallery {
      id
      data {
        body {
          ... on PrismicLandingGalleryBodySlideshow {
            id
            primary {
              interval_duration
            }
            slice_type
            items {
              image {
                fluid(maxWidth: 2500) {
                  ...GatsbyPrismicImageFluid
                }
              }
            }
          }
          ... on PrismicLandingGalleryBodyRandomImage {
            id
            slice_type
            items {
              image {
                fluid(maxWidth: 2500) {
                  ...GatsbyPrismicImageFluid
                }
              }
            }
          }
          ... on PrismicLandingGalleryBodyVideo {
            id
            slice_type
            primary {
              vimeo_link_id
            }
          }
        }
      }
    }
    allShopifyCollection(filter: { title: { eq: "ALL" } }) {
      edges {
        node {
          products {
            availableForSale
            id
            title
            handle
            createdAt
            images {
              id
              originalSrc
              localFile {
                childImageSharp {
                  fluid(maxWidth: 910, quality: 90) {
                    ...GatsbyImageSharpFluid
                  }
                }
              }
            }
            variants {
              price
              availableForSale
              title
            }
            availableForSale
          }
        }
      }
    }
    prismicProductShippingInformation {
      data {
        text {
          html
        }
      }
    }
  }
`

// allShopifyProduct {
//   edges {
//     node {
//       availableForSale
//       id
//       title
//       handle
//       createdAt
//       images {
//         id
//         originalSrc
//         localFile {
//           childImageSharp {
//             fluid(maxWidth: 910, quality: 90) {
//               ...GatsbyImageSharpFluid
//             }
//           }
//         }
//       }
//       variants {
//         price
//         availableForSale
//         title
//       }
//       availableForSale
//     }
//   }
// }
