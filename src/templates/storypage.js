import React from "react"

import styled from "styled-components"

import SEO from "../components/seo"

import Footer from "../components/footer"

import StoriesGrid from "../components/storiesgrid"

import { mergePrismicPreviewData } from "gatsby-source-prismic"

import {
  TitleBlock,
  TitleBlockWithImage,
  BigQuote,
  Image,
  Text,
  ImageGalleryX2,
  ImageGalleryX3,
  Video,
} from "../components/slices/index"

const IS_BROWSER = typeof window !== "undefined"

const Container = styled.div`
  font-family: "Arial Narrow";
  font-size: 1rem;
  background-color: ${props => props.backgroundcolour};
  color: ${props => props.textcolour};
`

const Spacer = styled.div`
  height: 150px;
  background-color: ${props => props.backgroundcolour};
`

class StoryPage extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
    setTimeout(() => {
      window.scrollTo(0, 0)
    }, 0)
  }

  renderSlices = slice => {
    switch (slice.slice_type) {
      case "title_block":
        return <TitleBlock data={slice.primary} />
        break
      case "title_block_with_image":
        return <TitleBlockWithImage data={slice.primary} />
        break
      case "big_quote":
        return <BigQuote data={slice.primary} />
        break
      case "image":
        return <Image data={slice.primary} />
        break
      case "text":
        return <Text data={slice.primary} />
        break
      case "image-gallery_x_2":
        return <ImageGalleryX2 data={slice.primary} />
      case "image-gallery_x_3":
        return <ImageGalleryX3 data={slice.primary} />
        break
      case "video":
        return <Video data={slice.primary} />
        break
      default:
        return null
    }
  }

  render() {
    let previewData = IS_BROWSER && window.__PRISMIC_PREVIEW_DATA__
    let staticData = this.props.data ? this.props.data.prismicStory.data : null

    if (typeof previewData === "object" && typeof window !== "undefined") {
      previewData = Object.values(previewData)[0].data
    } else if (typeof window !== "undefined") {
      if (window.__PRISMIC_PREVIEW_DATA)
        previewData = window.__PRISMIC_PREVIEW_DATA.prismicStory.data
    }

    // const data
    // if(previewData || staticData) {
    //   data = mergePrismicPreviewData({ staticData, previewData })
    // } else {
    //   data = null
    // }

    const data = mergePrismicPreviewData({ staticData, previewData })

    return (
      <>
        <SEO title={this.props.pageContext.title} />
        <Spacer backgroundcolour={data.background_colour} />
        <Container
          backgroundcolour={data.background_colour}
          textcolour={data.text_colour}
        >
          {data.body.map(slice => this.renderSlices(slice))}
          <StoriesGrid />
          <Footer />
        </Container>
      </>
    )
  }
}

export default StoryPage

export const storyQuery = graphql`
  query StoryById($id: String!) {
    prismicStory(id: { eq: $id }) {
      data {
        body {
          ... on PrismicStoryBodyVideo {
            id
            primary {
              text_field {
                html
              }
            }
            slice_type
          }
          ... on PrismicStoryBodyBigQuote {
            id
            primary {
              alignment
              quote {
                html
              }
            }
            slice_type
          }
          ... on PrismicStoryBodyImage {
            id
            primary {
              credits_alignment
              image_alignment
              image {
                localFile {
                  childImageSharp {
                    fluid(maxWidth: 2500) {
                      ...GatsbyImageSharpFluid
                    }
                  }
                }
              }
              credits {
                html
              }
            }
            slice_type
          }
          ... on PrismicStoryBodyImageGalleryX2 {
            id
            slice_type
            primary {
              image_one {
                alt
                localFile {
                  childImageSharp {
                    fluid(maxWidth: 1500) {
                      ...GatsbyImageSharpFluid
                    }
                  }
                }
              }
              image_two {
                alt
                localFile {
                  childImageSharp {
                    fluid(maxWidth: 1500) {
                      ...GatsbyImageSharpFluid
                    }
                  }
                }
              }
            }
          }
          ... on PrismicStoryBodyImageGalleryX3 {
            id
            primary {
              image_one {
                alt
                localFile {
                  childImageSharp {
                    fluid(maxWidth: 910) {
                      ...GatsbyImageSharpFluid
                    }
                  }
                }
              }
              image_two {
                alt
                localFile {
                  childImageSharp {
                    fluid(maxWidth: 910) {
                      ...GatsbyImageSharpFluid
                    }
                  }
                }
              }
              image_three {
                alt
                localFile {
                  childImageSharp {
                    fluid(maxWidth: 910) {
                      ...GatsbyImageSharpFluid
                    }
                  }
                }
              }
            }
            slice_type
          }
          ... on PrismicStoryBodyText {
            id
            primary {
              text_field {
                html
              }
              alignment
            }
            slice_type
          }
          ... on PrismicStoryBodyTitleBlock {
            id
            primary {
              credits {
                html
              }
              text {
                html
              }
              title {
                text
              }
            }
            slice_type
          }
          ... on PrismicStoryBodyTitleBlockWithImage {
            id
            slice_type
            primary {
              image_align
              credits {
                html
              }
              image {
                alt
                localFile {
                  childImageSharp {
                    fluid(maxWidth: 910) {
                      ...GatsbyImageSharpFluid
                    }
                  }
                }
              }
              text {
                html
              }
              title {
                text
              }
            }
          }
        }
        text_colour
        title {
          text
        }
        background_colour
      }
    }
  }
`
