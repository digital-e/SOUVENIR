import React from "react"
import { graphql } from "gatsby"
import styled from "styled-components"

import SEO from "../components/seo"

import Footer from "../components/footer"

import ProductsGrid from "../components/productsgrid"

import SouvenirTicker from "../components/souvenir-ticker"

const Spacer = styled.div`
  margin-top: 100px;
`

class ProductCategoryPage extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
    setTimeout(() => {
      window.scrollTo(0, 0)
    }, 450)
  }

  render() {
    return (
      <>
        <SEO title={this.props.pageContext.uri} />
        <Spacer />
        <ProductsGrid allProducts={this.props.data.allShopifyProduct} />
        <SouvenirTicker fixed={true} />
        <Footer />
      </>
    )
  }
}

export default ProductCategoryPage

// export const query = graphql`
//   query {
//     allShopifyProduct(sort: { fields: [createdAt], order: DESC }) {
//       edges {
//         node {
//           availableForSale
//           id
//           title
//           handle
//           createdAt
//           images {
//             id
//             originalSrc
//             localFile {
//               childImageSharp {
//                 fluid(maxWidth: 910) {
//                   ...GatsbyImageSharpFluid_noBase64
//                 }
//               }
//             }
//           }
//           variants {
//             price
//             availableForSale
//             title
//           }
//         }
//       }
//     }
//   }
// `
