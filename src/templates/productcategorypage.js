import React from "react"
import { graphql } from "gatsby"
import styled from "styled-components"

import SEO from "../components/seo"

import Footer from "../components/footer"

import ProductsGrid from "../components/productsgrid"

import SouvenirTicker from "../components/souvenir-ticker"

import CookieBanner from "../components/cookie-banner"

const Spacer = styled.div`
  margin-top: 100px;
`

class ProductCategoryPage extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
    setTimeout(() => {
      window.scrollTo(0, 0)
    }, 0)

    setTimeout(() => {
      if (this.fixedTickerRef) {
        this.fixedTickerRef.classList.remove("hide")
        this.fixedTickerRef.classList.add("show")
      }
    }, 2000)
  }

  toggleTicker = () => {
    this.fixedTickerRef.classList.remove("show")
    this.fixedTickerRef.classList.add("hide")
    setTimeout(() => {
      this.fixedTickerRef.style.display = "none"
    }, 1000)
  }

  render() {
    return (
      <>
        <SEO title={this.props.pageContext.title} />
        <Spacer />
        <ProductsGrid
          allProducts={this.props.data.shopifyCollection.products}
        />
        <SouvenirTicker fixed={true} />
        {/* <CookieBanner /> */}
        <Footer />
      </>
    )
  }
}

export default ProductCategoryPage

export const query = graphql`
  query($id: String!) {
    shopifyCollection(id: { eq: $id }) {
      products {
        id
        images {
          id
          originalSrc
          localFile {
            childImageSharp {
              fluid(maxWidth: 910, quality: 90) {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
        variants {
          availableForSale
          price
          title
        }
        availableForSale
        handle
        title
        createdAt
      }
    }
  }
`
