import React from "react"
import { graphql } from "gatsby"
import styled from "styled-components"
import Img from "gatsby-image"

import posed from "react-pose"

import Flickity from "flickity"

import "../components/flickity.css"

import SEO from "../components/seo"

import Footer from "../components/footer"

import SouvenirTicker from "../components/souvenir-ticker"

import ProductsGrid from "../components/productsgrid"

import ProductBuyContainer from "../components/productbuycontainer"

const Product = styled.div`
  display: flex;
  flex-direction: column;

  @media (min-width: 576px) {
    display: flex;
    flex-direction: row;
    max-height: 100vh;
  }
`

const ProductPanelLeft = styled.div`
  flex-basis: 50%;
`

const ProductPanelRight = styled.div`
  flex-basis: 50%;
  display: flex;
  justify-content: center;
  padding-top: 2rem;
  padding-bottom: 2rem;

  @media (min-width: 576px) {
    flex-basis: 50%;
    display: flex;
    justify-content: flex-start;
    padding-top: 0;
    padding-bottom: 0;
    overflow-y: scroll;
  }
`

class ProductPage extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      isOpenProductDetails: false,
      isOpenShipping: false,
      productPanelRightHeight: 0,
      productBuyContainerheight: 0,
      windowHeight: 0,
      windowWidth: 0,
    }

    this.productBuyContainerRef = React.createRef()

    this.elem = null
    this.flkty = null
  }

  componentDidMount() {
    setTimeout(() => {
      window.scrollTo(0, 0)
    }, 0)

    this.elem = this.carouselRef

    // setTimeout(() => {
    //   this.setState({
    //     productPanelRightHeight: this.productPanelRightRef
    //       ? this.productPanelRightRef.getBoundingClientRect().height
    //       : 0,
    //     // productBuyContainerHeight: this.productBuyContainerRef.getBoundingClientRect()
    //     //   .height,
    //   })
    // }, 0)

    this.setState({
      windowWidth: window.innerWidth,
      windowHeight: window.innerHeight,
    })

    setTimeout(() => {
      this.flkty = new Flickity(this.elem, {
        // options
        cellAlign: "left",
        // contain: true,
        initialIndex: 0,
        wrapAround: true,
        // adaptiveHeight: true,
      })
    }, 0)

    window.addEventListener("resize", () => {
      this.setState({
        windowWidth: window.innerWidth,
        windowHeight: window.innerHeight,
      })
    })
  }

  render() {
    let { windowHeight, windowWidth } = this.state

    return (
      <>
        <SEO title={this.props.data.shopifyProduct.title} />
        <Product>
          <ProductPanelLeft>
            <div ref={el => (this.carouselRef = el)} className="carousel">
              {this.props.data.shopifyProduct.images.length < 3
                ? this.props.data.shopifyProduct.images.map(item => (
                    <div className="carousel-cell">
                      <Img
                        style={{
                          width:
                            windowWidth > 576 ? windowWidth / 2 : windowWidth,
                          // height:
                          //   window.innerWidth > 576
                          //     ? ((item.localFile.childImageSharp.fluid
                          //         .presentationHeight /
                          //         item.localFile.childImageSharp.fluid
                          //           .presentationWidth) *
                          //         window.innerWidth) /
                          //       2
                          //     : (item.localFile.childImageSharp.fluid
                          //         .presentationHeight /
                          //         item.localFile.childImageSharp.fluid
                          //           .presentationWidth) *
                          //       window.innerWidth,
                          height:
                            windowWidth > 576
                              ? "100vh"
                              : (item.localFile.childImageSharp.fluid
                                  .presentationHeight /
                                  item.localFile.childImageSharp.fluid
                                    .presentationWidth) *
                                windowWidth,
                        }}
                        fluid={
                          item ? item.localFile.childImageSharp.fluid : null
                        }
                      />
                    </div>
                  ))
                : this.props.data.shopifyProduct.images.map((item, index) => {
                    if (index < 2) return
                    return (
                      <div className="carousel-cell">
                        <Img
                          style={{
                            width:
                              windowWidth > 576 ? windowWidth / 2 : windowWidth,
                            // height:
                            //   window.innerWidth > 576
                            //     ? ((item.localFile.childImageSharp.fluid
                            //         .presentationHeight /
                            //         item.localFile.childImageSharp.fluid
                            //           .presentationWidth) *
                            //         window.innerWidth) /
                            //       2
                            //     : (item.localFile.childImageSharp.fluid
                            //         .presentationHeight /
                            //         item.localFile.childImageSharp.fluid
                            //           .presentationWidth) *
                            //       window.innerWidth,
                            height:
                              windowWidth > 576
                                ? "100vh"
                                : (item.localFile.childImageSharp.fluid
                                    .presentationHeight /
                                    item.localFile.childImageSharp.fluid
                                      .presentationWidth) *
                                  windowWidth,
                          }}
                          fluid={
                            item ? item.localFile.childImageSharp.fluid : null
                          }
                        />
                      </div>
                    )
                  })}
            </div>
            {/* {this.props.data.shopifyProduct.images.length < 3
              ? this.props.data.shopifyProduct.images.map(item => (
                  <Img
                    fluid={item ? item.localFile.childImageSharp.fluid : null}
                  />
                ))
              : this.props.data.shopifyProduct.images.map((item, index) => {
                  if (index < 2) return
                  return (
                    <Img
                      fluid={item ? item.localFile.childImageSharp.fluid : null}
                    />
                  )
                })} */}
          </ProductPanelLeft>
          <ProductPanelRight ref={el => (this.productPanelRightRef = el)}>
            <ProductBuyContainer
              product={this.props.data.shopifyProduct}
              productPanelRightHeight={this.state.productPanelRightHeight}
              shippingInfo={
                this.props.data.prismicProductShippingInformation.data.text.html
              }
            />
          </ProductPanelRight>
        </Product>
        <SouvenirTicker fixed={false} />
        <SouvenirTicker fixed={true} />
        <ProductsGrid allProducts={this.props.data.allShopifyProduct} />
        <Footer />
      </>
    )
  }
}

export default ProductPage

export const query = graphql`
  query($handle: String!) {
    shopifyProduct(handle: { eq: $handle }) {
      id
      title
      handle
      productType
      tags
      descriptionHtml
      shopifyId
      options {
        id
        name
        values
      }
      variants {
        id
        title
        price
        availableForSale
        shopifyId
        selectedOptions {
          name
          value
        }
      }
      images {
        originalSrc
        id
        localFile {
          childImageSharp {
            fluid(maxWidth: 910) {
              presentationWidth
              presentationHeight
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }

    allShopifyProduct(sort: { fields: [createdAt], order: DESC }) {
      edges {
        node {
          availableForSale
          id
          title
          handle
          createdAt
          images {
            id
            originalSrc
            localFile {
              childImageSharp {
                fluid(maxWidth: 910, quality: 90) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
          variants {
            price
            availableForSale
            title
          }
          availableForSale
        }
      }
    }
    prismicProductShippingInformation {
      data {
        text {
          html
        }
      }
    }
  }
`

// export const query = graphql`
//   query($handle: String!) {
//     shopifyProduct(handle: { eq: $handle }) {
//       id
//       title
//       handle
//       productType
//       descriptionHtml
//       shopifyId
//       options {
//         id
//         name
//         values
//       }
//       variants {
//         id
//         title
//         price
//         availableForSale
//         shopifyId
//         selectedOptions {
//           name
//           value
//         }
//       }
//       images {
//         originalSrc
//         id
//         localFile {
//           childImageSharp {
//             fluid(maxWidth: 910, quality: 90) {
//               ...GatsbyImageSharpFluid_withWebp
//             }
//           }
//           childImageSharp {
//             fixed(width: 1200) {
//               ...GatsbyImageSharpFixed_withWebp
//             }
//           }
//         }
//       }
//     }

//     allShopifyProduct(sort: { fields: [createdAt], order: DESC }) {
//       edges {
//         node {
//           availableForSale
//           id
//           title
//           handle
//           createdAt
//           images {
//             id
//             originalSrc
//             localFile {
//               childImageSharp {
//                 fluid(maxWidth: 910, quality: 90) {
//                   ...GatsbyImageSharpFluid
//                 }
//               }
//             }
//           }
//           variants {
//             price
//             availableForSale
//             title
//           }
//           availableForSale
//         }
//       }
//     }
//     prismicProductShippingInformation {
//       data {
//         text {
//           html
//         }
//       }
//     }
//   }
// `
