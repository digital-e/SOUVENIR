import React from "react"

import styled from "styled-components"

import SEO from "../components/seo"

import Footer from "../components/footer"

import { mergePrismicPreviewData } from "gatsby-source-prismic"

const IS_BROWSER = typeof window !== "undefined"

const OuterDiv = styled.div`
  > div:first-child {
    padding-top: 10rem;
  }

  > div {
    padding: 3rem 0;
  }
`

const Container = styled.div`
  font-family: "Arial Narrow";
  font-size: 1rem;
  padding: 10rem 0;
  text-align: ${props => props.regularcenter};
  // min-height: 100vh;
  width: 80%;
  box-sizing: border-box;
  margin: 0 auto;

  @media (min-width: 576px) {
    width: 80%;
  }

  a {
    color: rgb(0, 0, 255);
    text-decoration: none;
  }

  strong {
    font-weight: bold !important;
  }

  p {
    line-height: 1.5rem;
  }

  a:hover {
    text-decoration: underline;
  }

  h1 {
    font-size: 1.5rem;
    text-align: ${props => props.hcenter};
  }
`

const Line = styled.div`
  height: 2px;
  width: 100%;
  background-color: grey;
  opacity: 0.3;
`

class Info extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
    setTimeout(() => {
      window.scrollTo(0, 0)
    }, 0)
  }

  render() {
    let previewData = IS_BROWSER && window.__PRISMIC_PREVIEW_DATA__
    let staticData = this.props.pageContext.data

    if (typeof previewData === "object" && typeof window !== "undefined") {
      previewData = Object.values(previewData)[0].data
    }

    const data = mergePrismicPreviewData({ staticData, previewData })

    return (
      <>
        <OuterDiv>
          <SEO title={this.props.pageContext.data.title.text} />
          <Container
            className="about"
            hcenter={data.about_h1_text_align}
            regularcenter={data.about_regular_text_align}
          >
            <div
              dangerouslySetInnerHTML={{
                __html: data.about.html,
              }}
            />
          </Container>
          {/* <Line /> */}
          <Container
            className="contact"
            hcenter={data.contact_h1_text_align}
            regularcenter={data.contact_regular_text_align}
          >
            <div
              dangerouslySetInnerHTML={{
                __html: data.contact.html,
              }}
            />
          </Container>
          {/* <Line /> */}
          <Container
            className="stockists"
            hcenter={data.berlin_store_h1_text_align}
            regularcenter={data.berlin_store_regular_text_align}
          >
            <div
              dangerouslySetInnerHTML={{
                __html: data.berlin_store.html,
              }}
            />
          </Container>
        </OuterDiv>
        <Footer />
      </>
    )
  }
}

export default Info
