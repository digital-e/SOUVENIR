import React from "react"
import styled from "styled-components"
import { Link } from "gatsby"

import { Slideshow, RandomImage, Video } from "./landing-gallery-slices/index"

// import StoriesGrid from "./storiesgrid"
import ProductsGrid from "./productsgrid"
import Footer from "./footer"

const Container = styled.div`
  height: 100vh;
  width: 100vw;
  object-fit: contain;
  position: relative;
  z-index: 998;
  cursor: pointer;

  .top {
    z-index: 998;
    visibility: visible;
  }
`

const Text = styled.div`
  position: absolute;
  z-index: 999;
  font-family: "Arial Narrow Bold";
  font-size: 10vw;
  color: black;
  // color: rgb(219, 217, 18);
  // -webkit-text-stroke: thin black;
  left: 50%;
  top: 50%;
  white-space: nowrap;
  transform: translate(-50%, -50%);
  width: 100vw;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  a {
    text-decoration: none;
    color: inherit;
    -webkit-text-stroke: inherit;
  }

  @media (min-width: 576px) {
    font-size: 8vw;
  }
`

class LandingGallery extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      galleryIsOpen: true,
    }
  }

  componentDidMount() {
    setTimeout(() => {
      if (this.titleRef) {
        this.titleRef.classList.add("show")
        this.titleRef.classList.remove("hide")
      }
    }, 500)
  }

  renderSlices = slice => {
    switch (slice.slice_type) {
      case "slideshow":
        return <Slideshow data={slice} />
        break
      case "random_image":
        return <RandomImage data={slice} />
        break
      case "video":
        return <Video data={slice} />
        break
      default:
        return null
    }
  }

  closeGallery = () => {
    this.setState({
      galleryIsOpen: false,
    })

    document.querySelector(".landing-products-grid-container").style.opacity = 1
    document.querySelector(".landing-products-grid-container").style.zIndex = 0

    setTimeout(() => {
      document.querySelector(
        ".landing-products-grid-container"
      ).style.position = "relative"
      document.querySelector(".landing-gallery").style.display = "none"
    }, 500)
  }

  render() {
    return (
      <div
        style={{ overflow: "hidden" }}
        className={
          this.state.galleryIsOpen
            ? "landing-gallery showGallery"
            : "landing-gallery hideGallery"
        }
      >
        {/* <Link to="/ALL/"> */}
        <Container onClick={() => this.closeGallery()}>
          <Text ref={el => (this.titleRef = el)} className="hide">
            SOUVENIR OFFICIAL
          </Text>
          {/* <Text ref={el => (this.titleRef = el)} className="hide">
              <Link to="/stories/">STORIES</Link>
              <Link to="/ALL/">SHOP</Link>
            </Text> */}
          {this.props.data.data.body.map(slice => this.renderSlices(slice))}
        </Container>
        {/* </Link> */}
        {/* <StoriesGrid /> */}
        <ProductsGrid allProducts={this.props.products} />
      </div>
    )
  }
}

export default LandingGallery
