import { Link, graphql, StaticQuery } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import styled from "styled-components"

import MobileCart from "./mobile-cart"

import posed from "react-pose"

import StoreContext from "../context/StoreContext"

const SouvenirLogo = styled.div`
  position: fixed;
  left: 50%;
  z-index: 997;
  font-family: "Arial Narrow Bold";

  transform: translateX(-50%);
  top: 2rem;
  font-size: 1.2rem;

  a {
    text-decoration: none;
    color: white;
  }
`

const preMenuContainer = posed.ul({
  openMenu: {
    opacity: 1,
    applyAtStart: { display: "flex" },
    transition: {
      ease: "easeInOut",
    },
  },
  closedMenu: {
    opacity: 0,
    applyAtEnd: { display: "none" },
    transition: {
      ease: "easeInOut",
    },
  },
})

const MenuContainer = styled(preMenuContainer)`
  position: fixed;
  z-index: 999;
  list-style: none;
  margin: 0;
  padding: 0;
  font-family: "Arial Narrow Bold";

  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  backdrop-filter: blur(10px);
  background-color: rgb(255, 255, 255, 0.5);

  > li:nth-child(n + 2) {
    margin-top: 10px;
  }
`

const preSubMenuContainer = posed.ul({
  open: {
    delayChildren: 50,
    staggerChildren: 10,
    maxHeight: 300,
    applyAtStart: { display: "block" },
    transition: {
      ease: "easeInOut",
    },
  },
  closed: {
    delay: 300,
    maxHeight: 0,
    applyAtEnd: { display: "none" },
    transition: {
      ease: "easeInOut",
    },
  },
})

const preCartInner = posed.div({
  open: {
    delayChildren: 50,
    staggerChildren: 10,
    opacity: 1,
    applyAtStart: { display: "block" },
    transition: {
      ease: "easeInOut",
    },
  },
  closed: {
    opacity: 0,
    delay: 300,
    applyAtEnd: { display: "none" },
    transition: {
      ease: "easeInOut",
    },
  },
})

const CartInner = styled(preCartInner)`
  height: 100%;
  width: 100%;
  overflow-y: scroll;
`

const SubMenuContainer = styled(preSubMenuContainer)`
  list-style: none;
  margin: 0;
  padding: 0;
  width: 100%;
  font-family: "Arial Narrow";

  > li {
    padding-top: 10px;
  }

  > li:last-child {
    padding-bottom: 50px;
  }
`

const SecondSubMenuContainer = styled(preSubMenuContainer)`
  list-style: none;
  margin: 0;
  padding: 0;
  width: 100%;
  font-family: "Arial Narrow";

  > li {
    padding-top: 10px;
  }

  > li:last-child {
    padding-bottom: 50px;
  }
`

const preListItem = posed.li({
  open: {
    y: 0,
    opacity: 1,
    transition: {
      ease: "easeInOut",
    },
  },
  closed: {
    opacity: 0,
    transition: {
      ease: "easeInOut",
    },
  },
})

const ListItem = styled(preListItem)`
  text-decoration: none;
  margin: 0;
  padding: 0;
  font-size: 2rem;
  text-align: center;
  width: 100%;

  :hover {
    text-decoration: underline;
    cursor: pointer;
  }
`

const ComingSoonListItem = styled(preListItem)`
  text-decoration: none;
  margin: 0;
  padding: 0;
  font-size: 2rem;
  text-align: center;
  width: 100%;
`

const ShoppingBagListItem = styled.li`
  text-decoration: none;
  margin: 0;
  padding-top: 2rem;
  font-size: 1rem;
  font-family: "Arial Narrow";
  text-align: center;
  width: 100%;

  :hover {
    text-decoration: underline;
    cursor: pointer;
  }
`

const ShippingListItem = styled.li`
  text-decoration: none;
  margin: 0;
  padding: 0;
  font-size: 1rem;
  font-family: "Arial Narrow";
  text-align: center;
  width: 100%;

  :hover {
    text-decoration: underline;
    cursor: pointer;
  }
`

const StyledLink = styled(Link)`
  color: black;
  text-decoration: none;

  :hover {
    text-decoration: underline;
    cursor: pointer;
  }
`

const preCartOuter = posed.div({
  open: {
    opacity: 1,
    applyAtStart: { display: "block" },
    transition: {
      ease: "easeInOut",
    },
  },
  closed: {
    opacity: 0,
    applyAtEnd: { display: "none" },
    transition: {
      ease: "easeInOut",
    },
  },
})

const CartOuter = styled(preCartOuter)`
  position: fixed;
  left: 50%;
  top: 50%;
  z-index: 999;
  font-family: "Arial Narrow";
  list-style: none;
  width: 100%;
  height: 100%;

  transform: translate(-50%, -50%);
  backdrop-filter: blur(10px);
  background-color: rgb(255, 255, 255, 0.5);

  li {
    width: 100%;
    display: flex;
    justify-content: flex-end;
  }

  li:last-child {
    margin-top: 10px;
  }
`
const Container = styled.div`
  z-index: 997;
`

const CartContainer = styled.div`
  height: 100%;
  width: 100%;
  position: fixed;
  z-index: 999;
`

const CartButton = styled.div`
  position: fixed;
  font-family: "Arial Narrow";
  font-size: 1.2rem;
  top: 2rem;
  right: 0;
  transform-origin: top right;
  z-index: 997;
  margin-right: 2rem;
`

const MenuButton = styled.div`
  position: fixed;
  font-family: "Arial Narrow Bold";
  font-size: 1.2rem;
  bottom: 0;
  left: 50%;
  transform: translateX(-50%);
  z-index: 997;
  margin-bottom: 45px;
`

class Menu extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      menuIsOpen: false,
      secondMenuIsOpen: false,
      subMenuIsOpen: false,
      cartIsOpen: false,
      hoverComingSoon: false,
    }

    this.toggleSubMenu = this.toggleSubMenu.bind(this)
    this.toggleSecondSubMenu = this.toggleSecondSubMenu.bind(this)
    this.closeSubMenu = this.closeSubMenu.bind(this)
    this.closeSecondSubMenu = this.closeSecondSubMenu.bind(this)

    this.toggleMenu = this.toggleMenu.bind(this)
    this.closeMenu = this.closeMenu.bind(this)

    this.toggleCart = this.toggleCart.bind(this)
    this.toggleCartMenu = this.toggleCartMenu.bind(this)

    this.context = null
  }

  componentDidMount() {
    this.context = this.context

    if (window.innerWidth < 576) {
      setTimeout(() => {
        this.containerRef.classList.remove("hide")
        this.containerRef.classList.add("show")
      }, 1000)
    }
  }

  toggleSubMenu() {
    this.closeSecondSubMenu()
    this.setState(prevState => ({
      subMenuIsOpen: !prevState.subMenuIsOpen,
    }))
  }

  toggleSecondSubMenu() {
    this.closeSubMenu()
    this.setState(prevState => ({
      secondMenuIsOpen: !prevState.secondMenuIsOpen,
    }))
  }

  toggleMenu() {
    this.setState(prevState => ({
      menuIsOpen: !prevState.menuIsOpen,
    }))
  }

  closeSubMenu() {
    this.setState({
      subMenuIsOpen: false,
    })
  }

  closeSecondSubMenu() {
    this.setState({
      secondMenuIsOpen: false,
    })
  }

  closeMenu() {
    this.setState({
      menuIsOpen: false,
    })
  }

  toggleCart() {
    this.setState(prevState => ({
      cartIsOpen: !prevState.cartIsOpen,
    }))
  }

  toggleCartMenu() {
    this.closeMenu()
    this.toggleCart()
  }

  quantityInCart() {
    let total = 0
    this.context.checkout.lineItems.forEach(item => {
      let current = null
      current = item.quantity
      total += current
    })

    return total
  }

  toggleComingSoon = () => {
    this.setState(prevState => ({
      hoverComingSoon: !prevState.hoverComingSoon,
    }))
  }

  render() {
    return (
      <div ref={el => (this.containerRef = el)} className="hide">
        <div
          style={{
            mixBlendMode: "difference",
            zIndex: 999,
            position: "fixed",
            webkitFontSmoothing: "antialiased",
            mozOsxFontSmoothing: "grayscale",
          }}
        >
          <MenuButton onClick={this.toggleMenu} style={{ color: "white" }}>
            MENU
          </MenuButton>
          <SouvenirLogo>
            <Link to="/">SOUVENIR OFFICIAL</Link>
          </SouvenirLogo>
        </div>
        <CartButton onClick={this.toggleCart}>
          {this.quantityInCart()}
        </CartButton>
        <Container>
          <MenuContainer
            pose={this.state.menuIsOpen ? "openMenu" : "closedMenu"}
          >
            <ListItem onClick={this.toggleSubMenu}>SHOP</ListItem>
            <SubMenuContainer
              pose={this.state.subMenuIsOpen ? "open" : "closed"}
            >
              {/* <ListItem onClick={this.closeMenu}>
                <StyledLink activeClassName="active-menu-element" to="/all">
                  ALL
                </StyledLink>
              </ListItem> */}
              {this.props.data.allShopifyCollection.edges.map(item => (
                <ListItem onClick={this.closeMenu}>
                  <StyledLink
                    activeClassName="active-menu-element"
                    to={item.node.title}
                  >
                    {item.node.title}
                  </StyledLink>
                </ListItem>
              ))}
            </SubMenuContainer>
            <ListItem
              // onClick={this.toggleSecondSubMenu}
              onClick={this.closeMenu}
            >
              <StyledLink activeClassName="active-menu-element" to="/info">
                INFO
              </StyledLink>
            </ListItem>
            {/* <SecondSubMenuContainer
              pose={this.state.secondMenuIsOpen ? "open" : "closed"}
            >
              <ListItem onClick={this.closeMenu}>
                <StyledLink activeClassName="active-menu-element" to="/about">
                  ABOUT
                </StyledLink>
              </ListItem>
              <ListItem onClick={this.closeMenu}>
                <StyledLink activeClassName="active-menu-element" to="/contact">
                  CONTACT
                </StyledLink>
              </ListItem>
              <ListItem onClick={this.closeMenu}>
                <StyledLink
                  activeClassName="active-menu-element"
                  to="/berlin-store"
                >
                  BERLIN STORE
                </StyledLink>
              </ListItem>
            </SecondSubMenuContainer> */}
            {/* <ListItem onClick={this.closeSubMenu} onClick={this.closeMenu}>
              <StyledLink activeClassName="active-menu-element" to="/stockists">
                STOCKISTS
              </StyledLink>
            </ListItem> */}
            {/* <ComingSoonListItem
            onClick={this.toggleComingSoon}
            > */}
            <ListItem onClick={this.closeMenu}>
              <StyledLink activeClassName="active-menu-element" to="/stories">
                {this.state.hoverComingSoon ? (
                  <span style={{ color: "rgb(255, 55, 252)" }}>
                    COMING SOON
                  </span>
                ) : (
                  "STORIES"
                )}
              </StyledLink>
            </ListItem>
            {/* </ComingSoonListItem> */}
            <ShoppingBagListItem
              activeClassName="active-menu-element"
              onClick={this.toggleCartMenu}
            >
              SHOPPING BAG: {this.quantityInCart()}
            </ShoppingBagListItem>
            <ShippingListItem onClick={this.closeMenu}>
              <StyledLink
                activeClassName="active-menu-element"
                to="/shipping-and-returns"
              >
                SHIPPING AND RETURNS
              </StyledLink>
            </ShippingListItem>
            <MenuButton onClick={this.toggleMenu}>CLOSE</MenuButton>
          </MenuContainer>
          <CartOuter pose={this.state.cartIsOpen ? "open" : "closed"}>
            <CartInner pose={this.state.cartIsOpen ? "open" : "closed"}>
              <MobileCart
                quantityInCart={this.quantityInCart()}
                pose={this.state.cartIsOpen ? "open" : "closed"}
              />
            </CartInner>
            <MenuButton onClick={this.toggleCart}>CLOSE</MenuButton>
          </CartOuter>
        </Container>
      </div>
    )
  }
}

Menu.propTypes = {
  siteTitle: PropTypes.string,
}

Menu.defaultProps = {
  siteTitle: ``,
}

Menu.contextType = StoreContext

export default () => (
  <StaticQuery
    query={graphql`
      query MobileCollectionsQuery {
        allShopifyCollection(sort: { fields: description, order: ASC }) {
          edges {
            node {
              title
            }
          }
        }
      }
    `}
    render={data => <Menu data={data} />}
  />
)
