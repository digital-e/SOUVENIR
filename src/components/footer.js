import { Link, graphql, StaticQuery } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import styled from "styled-components"

const FooterContainer = styled.ul`
  display: flex;
  flex-direction: column;
  position: relative;
  z-index: 990;
  list-style: none;
  margin: 0;
  padding: 6rem 0 2rem 0;
  font-family: "Arial Narrow";
  width: 100%;

  @media (min-width: 576px) {
    flex-direction: row;
  }
`

const ListItem = styled.li`
  text-decoration: none;
  margin: 0;
  padding: 0;
  font-size: 0.8rem;
  width: fit-content;

  a {
    text-decoration: none;
    color: black;
  }

  :hover {
    text-decoration: underline;
    cursor: pointer;
  }
`

const StyledLink = styled(Link)`
  color: black;
  text-decoration: none;

  :hover {
    text-decoration: underline;
    cursor: pointer;
  }
`

const Left = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  flex-basis: 33.3%;

  margin-bottom: 2rem;

  li {
    margin: 0 auto;
    margin-bottom: 0.5rem;
  }

  @media (min-width: 576px) {
    flex-direction: row;
    margin-bottom: 0;

    li {
      margin: 0 auto;
      margin-bottom: 0;
    }
  }
`

const Right = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  flex-basis: 33.3%;

  margin-bottom: 2rem;

  li {
    margin: 0 auto;
    margin-bottom: 0.5rem;
  }

  @media (min-width: 576px) {
    flex-direction: row;
    margin-bottom: 0;

    li {
      margin: 0 auto;
      margin-bottom: 0;
    }
  }
`

const Center = styled.div`
  z-index: 999;
  list-style: none;
  order: 3;
  margin: 0;
  padding: 0;
  font-family: "Arial Narrow Bold";
  flex-basis: 33.3%;

  @media (min-width: 576px) {
    flex-direction: row;
    order: 0;
  }

  li {
    margin: 0 auto;
  }
`

const Icons = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  height: 0.8rem;
  padding-bottom: 6rem;
`

const IconContainer = styled.div`
  height: 100%;
  width: auto;
  margin-left: 1rem;

  img {
    height: 100%;
  }
`

const Footer = props => {
  return (
    <>
      <FooterContainer>
        <Left>
          <ListItem>
            <StyledLink to="/imprint">Imprint</StyledLink>
          </ListItem>
          <ListItem>
            <StyledLink to="/terms-of-use">Terms Of Use</StyledLink>
          </ListItem>
          <ListItem>
            <StyledLink to="/privacy-policy">Privacy Policy</StyledLink>
          </ListItem>
        </Left>
        <Center>
          <ListItem>
            <StyledLink to="/">©2020, Souvenir Official</StyledLink>
          </ListItem>
        </Center>
        <Right>
          <ListItem>
            <StyledLink to="/frequently-asked-questions">FAQ</StyledLink>
          </ListItem>
          <ListItem>
            <StyledLink to="/contact">Contact</StyledLink>
          </ListItem>
          <ListItem>
            <a
              href="https://www.instagram.com/souvenir_official/"
              target="_blank"
            >
              Instagram
            </a>
          </ListItem>
        </Right>
      </FooterContainer>
      <Icons>
        <IconContainer>
          <img src={props.data.One.publicURL} />
        </IconContainer>
        <IconContainer>
          <img src={props.data.Two.publicURL} />
        </IconContainer>
        <IconContainer>
          <img src={props.data.Three.publicURL} />
        </IconContainer>
        <IconContainer>
          <img src={props.data.Four.publicURL} />
        </IconContainer>
        <IconContainer>
          <img src={props.data.Five.publicURL} />
        </IconContainer>
        <IconContainer>
          <img src={props.data.Six.publicURL} />
        </IconContainer>
        <IconContainer>
          <img src={props.data.Seven.publicURL} />
        </IconContainer>
      </Icons>
    </>
  )
}

export default props => (
  <StaticQuery
    query={graphql`
      query {
        One: file(relativePath: { eq: "icons/apple.svg" }) {
          publicURL
        }
        Two: file(relativePath: { eq: "icons/google.svg" }) {
          publicURL
        }
        Three: file(relativePath: { eq: "icons/maestro.svg" }) {
          publicURL
        }
        Four: file(relativePath: { eq: "icons/mastercard.svg" }) {
          publicURL
        }
        Five: file(relativePath: { eq: "icons/paypal.svg" }) {
          publicURL
        }
        Six: file(relativePath: { eq: "icons/shopify.png" }) {
          publicURL
        }
        Seven: file(relativePath: { eq: "icons/visa.png" }) {
          publicURL
        }
      }
    `}
    render={data => <Footer data={data} {...props} />}
  />
)
