import { Link, graphql, StaticQuery } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import styled from "styled-components"

import SouvenirSignetImg from "../images/SouvenirOfficial_logo_signet.svg"

import Cart from "../components/cart"

import posed from "react-pose"

import StoreContext from "../context/StoreContext"

const SouvenirLogo = styled.div`
  position: fixed;
  left: 50%;
  z-index: 999;
  font-family: "Arial Narrow Bold";
  transform: translateX(-50%);
  top: 2rem;
  font-size: 1.2rem;

  a {
    text-decoration: none;
    color: white;
  }
`
const SouvenirSignet = styled.img`
  position: fixed;
  left: 50%;
  z-index: 999;
  font-family: "Arial Narrow Bold";
  transform: translateX(-50%);
  bottom: 2rem;
  font-size: 1.2rem;
  width: 170px;
`

const MenuContainer = styled.ul`
  position: fixed;
  z-index: 999;
  top: 2rem;
  left: 2rem;
  list-style: none;
  margin: 0;
  padding: 0;
  font-family: "Arial Narrow Bold";
  width: fit-content;

  > li:nth-child(n + 2) {
    margin-top: 10px;
  }
`

const preSubMenuContainer = posed.ul({
  open: {
    delayChildren: 50,
    staggerChildren: 10,
    maxHeight: 300,
    applyAtStart: { display: "block" },
    transition: {
      ease: "easeInOut",
    },
  },
  closed: {
    delay: 300,
    maxHeight: 0,
    applyAtEnd: { display: "none" },
    transition: {
      ease: "easeInOut",
    },
  },
})

const CartInner = posed.div({
  open: {
    delayChildren: 50,
    staggerChildren: 10,
    height: "calc(100vh - 2rem - 46px)",
    applyAtStart: { display: "block" },
    transition: {
      ease: "easeInOut",
    },
  },
  closed: {
    delay: 300,
    height: 0,
    applyAtEnd: { display: "none" },
    transition: {
      ease: "easeInOut",
    },
  },
})

const SubMenuContainer = styled(preSubMenuContainer)`
  list-style: none;
  margin: 0;
  padding: 0;
  width: fit-content;
  font-family: "Arial Narrow";
  font-weight: normal;

  > li {
    padding-top: 10px;
  }

  > li:last-child {
    padding-bottom: 50px;
  }
`

const SecondSubMenuContainer = styled(preSubMenuContainer)`
  list-style: none;
  margin: 0;
  padding: 0;
  width: fit-content;
  font-family: "Arial Narrow";
  font-weight: normal;

  > li {
    padding-top: 10px;
  }

  > li:last-child {
    padding-bottom: 50px;
  }
`

const preListItem = posed.li({
  open: {
    y: 0,
    opacity: 1,
    transition: {
      ease: "easeInOut",
    },
  },
  closed: {
    opacity: 0,
    transition: {
      ease: "easeInOut",
    },
  },
})

const ListItem = styled(preListItem)`
  text-decoration: none;
  margin: 0;
  padding: 0;
  font-size: 0.8rem;
  width: fit-content;
  color: black;

  :hover {
    text-decoration: underline;
    cursor: pointer;
  }
`

const ComingSoonListItem = styled(preListItem)`
  text-decoration: none;
  margin: 0;
  padding: 0;
  font-size: 0.8rem;
  width: fit-content;
  cursor: pointer;
`

const StyledLink = styled(Link)`
  color: black;
  text-decoration: none;

  :hover {
    text-decoration: underline;
    cursor: pointer;
  }
`

const preCartOuter = posed.div({
  abierto: {
    backgroundColor: "rgba(255,255,255,1)",
    transition: {
      ease: "easeInOut",
    },
  },
  cerrado: {
    delay: 300,
    backgroundColor: "rgba(255,255,255,0)",
    transition: {
      ease: "easeInOut",
    },
  },
})

const CartOuter = styled(preCartOuter)`
  position: fixed;
  right: 0;
  top: 0;
  z-index: 999;
  font-family: "Arial Narrow";
  list-style: none;
  width: fit-content;
  font-weight: normal;
  background-color: white;
  padding-top: 2rem;

  li {
    width: 100%;
    display: flex;
    justify-content: flex-end;
    position: relative;
    right: 4rem;
    padding-left: 2rem;
  }

  li:last-child {
    margin-top: 10px;
  }

  > div {
    padding-right: 2rem;
    padding-left: 2rem;
  }
`
const Container = styled.div`
  z-index: 997;
  position: absolute;
`

class Menu extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      menuIsOpen: false,
      secondMenuIsOpen: false,
      cartIsOpen: false,
      hoverComingSoon: false,
    }

    this.toggleSubMenu = this.toggleSubMenu.bind(this)
    this.toggleSecondSubMenu = this.toggleSecondSubMenu.bind(this)
    this.closeSubMenu = this.closeSubMenu.bind(this)
    this.closeSecondSubMenu = this.closeSecondSubMenu.bind(this)

    this.toggleCart = this.toggleCart.bind(this)

    this.context = null
  }

  componentDidMount() {
    this.context = this.context

    if (window.innerWidth > 576) {
      setTimeout(() => {
        this.containerRef.classList.remove("hide")
        this.containerRef.classList.add("show")
      }, 1000)
    }

    setTimeout(() => {
      let offsetTop
      if (window.location.hash.length > 0) {
        this.setState({
          secondMenuIsOpen: true,
        })

        // this.scrollTo(window.location.hash.split("#")[1])
        offsetTop = document.querySelector(
          `.${window.location.hash.split("#")[1]}`
        ).offsetTop
        window.scrollTo({ top: offsetTop, left: 0, behavior: "smooth" })
      }
    }, 1000)
  }

  toggleSubMenu() {
    this.closeSecondSubMenu()
    this.setState(prevState => ({
      menuIsOpen: !prevState.menuIsOpen,
    }))
  }

  toggleSecondSubMenu() {
    this.closeSubMenu()
    this.setState(prevState => ({
      secondMenuIsOpen: !prevState.secondMenuIsOpen,
    }))
  }

  closeSubMenu() {
    this.setState({
      menuIsOpen: false,
    })
  }

  closeSecondSubMenu() {
    this.setState({
      secondMenuIsOpen: false,
    })
  }

  toggleCart() {
    this.setState(prevState => ({
      cartIsOpen: !prevState.cartIsOpen,
    }))
  }

  quantityInCart() {
    let total = 0
    this.context.checkout.lineItems.forEach(item => {
      let current = null
      current = item.quantity
      total += current
    })

    return total
  }

  toggleComingSoon = () => {
    this.setState(prevState => ({
      hoverComingSoon: !prevState.hoverComingSoon,
    }))
  }

  addHash = selector => {
    window.location = `/info#${selector}`
  }

  scrollTo = selector => {
    if (
      window.location.pathname !== "/info" &&
      window.location.pathname !== "/info/"
    ) {
      this.addHash(selector)
    } else {
      let offsetTop
      switch (selector) {
        case "about":
          offsetTop = document.querySelector(".about").offsetTop
          window.history.pushState("", "About", "#about")
          break
        case "contact":
          offsetTop = document.querySelector(".contact").offsetTop
          window.history.pushState("", "Contact", "#contact")
          break
        case "stockists":
          offsetTop = document.querySelector(".stockists").offsetTop
          window.history.pushState("", "Stockists", "#stockists")
          break
        default:
          offsetTop = document.querySelector(".about").offsetTop
      }

      window.scrollTo({ top: offsetTop, left: 0, behavior: "smooth" })
    }
  }

  render() {
    return (
      <>
        <div
          style={{
            mixBlendMode: "difference",
            zIndex: 999,
            position: "fixed",
            webkitFontSmoothing: "antialiased",
            mozOsxFontSmoothing: "grayscale",
          }}
        >
          <SouvenirSignet src={SouvenirSignetImg} />
        </div>
        <div
          style={{
            mixBlendMode: "difference",
            zIndex: 999,
            position: "fixed",
            webkitFontSmoothing: "antialiased",
            mozOsxFontSmoothing: "grayscale",
          }}
        >
          <SouvenirLogo>
            <Link to="/">SOUVENIR OFFICIAL</Link>
          </SouvenirLogo>
        </div>
        <Container ref={el => (this.containerRef = el)} className="hide">
          <CartOuter pose={this.state.cartIsOpen ? "abierto" : "cerrado"}>
            <ListItem onClick={this.toggleCart}>
              SHOPPING BAG: {this.quantityInCart()}
            </ListItem>
            <CartInner
              pose={this.state.cartIsOpen ? "open" : "closed"}
              style={{ overflowY: "scroll" }}
            >
              <Cart
                quantityInCart={this.quantityInCart()}
                pose={this.state.cartIsOpen ? "open" : "closed"}
              />
            </CartInner>
            <ListItem>
              <StyledLink
                activeClassName="active-menu-element"
                to="/shipping-and-returns"
              >
                SHIPPING AND RETURNS
              </StyledLink>
            </ListItem>
          </CartOuter>
          <MenuContainer>
            <ListItem onClick={this.toggleSubMenu}>
              <StyledLink to="/ALL">SHOP</StyledLink>
            </ListItem>
            <SubMenuContainer pose={this.state.menuIsOpen ? "open" : "closed"}>
              {/* <ListItem>
              <StyledLink activeClassName="active-menu-element" to="/all">
                ALL
              </StyledLink>
            </ListItem> */}
              {this.props.data.allShopifyCollection.edges.map(item => (
                <ListItem>
                  <StyledLink
                    activeClassName="active-menu-element"
                    to={item.node.title}
                  >
                    {item.node.title}
                  </StyledLink>
                </ListItem>
              ))}
            </SubMenuContainer>
            <ListItem
              // activeClassName="active-menu-element"
              // onClick={this.closeSubMenu}
              onClick={this.toggleSecondSubMenu}
            >
              <StyledLink to="/info">INFO</StyledLink>
            </ListItem>
            <SecondSubMenuContainer
              pose={this.state.secondMenuIsOpen ? "open" : "closed"}
            >
              <ListItem onClick={() => this.scrollTo("about")}>ABOUT</ListItem>
              <ListItem onClick={() => this.scrollTo("contact")}>
                CONTACT
              </ListItem>
              <ListItem onClick={() => this.scrollTo("stockists")}>
                STOCKISTS
              </ListItem>
            </SecondSubMenuContainer>
            <ComingSoonListItem
              onClick={() => {
                this.closeSubMenu()
                this.closeSecondSubMenu()
              }}
              // onMouseEnter={this.toggleComingSoon}
              // onMouseLeave={this.toggleComingSoon}
            >
              <StyledLink activeClassName="active-menu-element" to="/stories">
                {this.state.hoverComingSoon ? (
                  <span style={{ color: "rgb(255, 55, 252)" }}>
                    COMING SOON
                  </span>
                ) : (
                  "STORIES"
                )}
              </StyledLink>
            </ComingSoonListItem>
          </MenuContainer>
        </Container>
      </>
    )
  }
}

Menu.propTypes = {
  siteTitle: PropTypes.string,
}

Menu.defaultProps = {
  siteTitle: ``,
}

Menu.contextType = StoreContext

export default () => (
  <StaticQuery
    query={graphql`
      query CollectionsQuery {
        allShopifyCollection(sort: { fields: description, order: ASC }) {
          edges {
            node {
              title
            }
          }
        }
      }
    `}
    render={data => <Menu data={data} />}
  />
)
