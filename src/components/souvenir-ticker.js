import React, { useState, useEffect } from "react"
import { useStaticQuery, graphql } from "gatsby"

import styled from "styled-components"

import Ticker from "react-ticker"

const FixedTicker = styled.div`
  position: fixed;
  top: 0;
  z-index: 997;
  width: 100%;
  height: auto;
  cursor: pointer;
  line-height: 1;
`

const FixedTickerStatic = styled.div`
  height: auto;
  font-size: 0.9rem;
  font-family: "Arial Narrow";
  background: black;
  color: white;
  padding: 0.3rem 1rem;
  text-align: center;
`

const StandardTicker = styled.div``

const SouvenirTicker = ({ ...props }) => {
  const data = useStaticQuery(graphql`
    query {
      prismicAnnouncementBanner {
        data {
          announcement {
            text
          }
        }
      }
      prismicStandardBanner {
        data {
          announcement {
            text
          }
        }
      }
    }
  `)

  const [toggleTicker, setToggleTicker] = useState(false)

  useEffect(() => {
    let ticker = document.querySelector(".fixed-ticker")
    if (!props.fixed || window.sessionStorage.getItem("fixedTickerHasShown")) {
      ticker.classList.remove("hide-ticker")
      ticker.classList.add("show-ticker")
      return
    }

    setTimeout(() => {
      setToggleTicker(true)
      ticker.classList.remove("hide-ticker")
      ticker.classList.add("show-ticker")
    }, 2000)
    //Check if Fixed Ticker has already shown
    window.sessionStorage.setItem("fixedTickerHasShown", true)
  }, [])

  const handleClick = () => {
    // if (!props.fixed) return
    // let ticker = document.querySelector(".fixed-ticker")
    // setToggleTicker(false)
    // ticker.classList.add("hide")
    // ticker.classList.remove("show")
    // setTimeout(() => {
    //   ticker.style.display = "none"
    // }, 1000)
  }

  return props.fixed ? (
    <FixedTicker onClick={handleClick} className={"fixed-ticker hide-ticker"}>
      {data.prismicAnnouncementBanner.data.announcement.text ? (
        // <Ticker>
        //   {({ index }) => (
        //     <h1>{data.prismicAnnouncementBanner.data.announcement.text}</h1>
        //   )}
        // </Ticker>
        <FixedTickerStatic>
          {data.prismicAnnouncementBanner.data.announcement.text}
        </FixedTickerStatic>
      ) : null}
    </FixedTicker>
  ) : (
    <StandardTicker className={"standard-ticker"}>
      {data.prismicStandardBanner.data.announcement.text ? (
        <Ticker>
          {({ index }) => (
            <h1>{data.prismicStandardBanner.data.announcement.text}</h1>
          )}
        </Ticker>
      ) : null}
    </StandardTicker>
  )
}

export default SouvenirTicker
