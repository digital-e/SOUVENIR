import React, { useState, useContext, useEffect, useRef } from "react"
import PropTypes from "prop-types"
import styled from "styled-components"

import StoreContext from "../../context/StoreContext"
import VariantSelector from "./variantSelector"

const ProductTitle = styled.div`
  font-family: "Arial Narrow Bold";
  font-size: 1rem;
`

const ProductPrice = styled.div`
  font-family: "Arial Narrow";
  font-size: 1rem;
`

const SoldOut = styled.p`
  font-family: "Arial Narrow";
  padding-top: 1rem;
  color: rgb(0, 0, 255);
  opacity: 0.5;
  cursor: not-allowed;
`

const AddButton = styled.button`
  font-family: "Arial Narrow";
  display: block;
  border: 0;
  background: transparent;
  font-size: 1rem;
  margin-top: 1rem;
  outline: none;
  color: rgb(0, 0, 255);

  cursor: pointer;
`

const ProductForm = props => {
  const [quantity, setQuantity] = useState(1)
  const [variant, setVariant] = useState(props.product.variants[0])
  const context = useContext(StoreContext)
  let firstSelection = true

  const hasVariants = props.product.variants.length > 1
  const productVariant =
    context.client.product.helpers.variantForOptions(props.product, variant) ||
    variant
  const [available, setAvailable] = useState(productVariant.availableForSale)

  useEffect(() => {
    let defaultOptionValues = {}
    props.product.options.forEach(selector => {
      defaultOptionValues[selector.name] = selector.values[0]
    })
    setVariant(defaultOptionValues)

    setTimeout(() => {
      setAvailable(false)
    }, 1000)
  }, [])

  useEffect(() => {
    checkAvailability(props.product.shopifyId)
  }, [productVariant])

  const checkAvailability = productId => {
    context.client.product.fetch(productId).then(product => {
      // this checks the currently selected variant for availability
      const result = product.variants.filter(
        variant => variant.id === productVariant.shopifyId
      )

      setAvailable(result[0].available)
    })
  }

  const handleQuantityChange = event => {
    setQuantity(event.target.value)
  }

  const handleOptionChange = event => {
    const { target } = event
    //On first selection
    if (firstSelection) {
      props.product.variants[0].availableForSale
        ? setAvailable(true)
        : setAvailable(false)
      firstSelection = false
    }
    if (target.value === "select") return
    setVariant(prevState => ({
      ...prevState,
      [target.name]: target.value,
    }))
  }

  const handleAddToCart = () => {
    context.addVariantToCart(productVariant.shopifyId, quantity)
  }

  const variantSelectors = hasVariants
    ? props.product.options.map(option => {
        return (
          <VariantSelector
            onChange={handleOptionChange}
            key={option.id.toString()}
            option={option}
            variants={props.product.variants}
          />
        )
      })
    : null

  return (
    <>
      <ProductTitle>{props.product.title}</ProductTitle>
      <ProductPrice>{productVariant.price} EUR</ProductPrice>
      {variantSelectors}
      {/* <label htmlFor="quantity">Quantity </label>
      <input
        type="number"
        id="quantity"
        name="quantity"
        min="1"
        step="1"
        onChange={handleQuantityChange}
        value={quantity}
      /> */}
      <br />
      {available && (
        <AddButton
          type="submit"
          disabled={!available || context.adding}
          onClick={handleAddToCart}
        >
          ADD TO BAG
        </AddButton>
      )}

      {/* {!available && <SoldOut>THIS PRODUCT IS SOLD OUT</SoldOut>} */}
      {!available && <SoldOut>ADD TO BAG</SoldOut>}
    </>
  )
}

ProductForm.propTypes = {
  product: PropTypes.shape({
    descriptionHtml: PropTypes.string,
    handle: PropTypes.string,
    id: PropTypes.string,
    shopifyId: PropTypes.string,
    images: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string,
        originalSrc: PropTypes.string,
      })
    ),
    options: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string,
        name: PropTypes.string,
        values: PropTypes.arrayOf(PropTypes.string),
      })
    ),
    productType: PropTypes.string,
    title: PropTypes.string,
    variants: PropTypes.arrayOf(
      PropTypes.shape({
        availableForSale: PropTypes.bool,
        id: PropTypes.string,
        price: PropTypes.string,
        title: PropTypes.string,
        shopifyId: PropTypes.string,
        selectedOptions: PropTypes.arrayOf(
          PropTypes.shape({
            name: PropTypes.string,
            value: PropTypes.string,
          })
        ),
      })
    ),
  }),
  addVariantToCart: PropTypes.func,
}

export default ProductForm
