import React, { useEffect } from "react"
import { graphql, useStaticQuery } from "gatsby"

import { GridItemFull, GridItemHalf, GridItemQuarter } from "./GridItems/index"

import styled from "styled-components"

import Masonry from "masonry-layout"

const Grid = styled.div`
  * {
    box-sizing: border-box;
  }

  .grid {
    background: #eee;
    max-width: 1200px;
  }

  /* clearfix */

  .grid:after {
    content: "";
    display: block;
    clear: both;
  }

  /* ---- grid-item ---- */

  .image {
    height: 100%;
    width: 100%;

    img {
      object-fit: cover;
      height: 100%;
      width: 100%;
    }
  }
`

const renderGridItem = item => {
  switch (item.node.data.thumbnail_format) {
    case "Quarter Width":
      return <GridItemQuarter data={item.node} />
      break
    case "Half Width":
      return <GridItemHalf data={item.node} />
      break
    case "Full Width":
      return <GridItemFull data={item.node} />
      break
    default:
      return null
  }
}

const StoriesGrid = props => {
  const data = useStaticQuery(graphql`
    query {
      allPrismicStory(
        filter: { uid: { ne: "placeholderslices" } }
        sort: { fields: data___order, order: DESC }
      ) {
        edges {
          node {
            data {
              thumbnail {
                url
                localFile {
                  childImageSharp {
                    fluid {
                      base64
                      tracedSVG
                      aspectRatio
                      src
                      srcSet
                      srcWebp
                      srcSetWebp
                      sizes
                      originalImg
                      originalName
                      presentationWidth
                      presentationHeight
                    }
                  }
                }
              }
              thumbnail_mobile {
                url
                localFile {
                  childImageSharp {
                    fluid {
                      base64
                      tracedSVG
                      aspectRatio
                      src
                      srcSet
                      srcWebp
                      srcSetWebp
                      sizes
                      originalImg
                      originalName
                      presentationWidth
                      presentationHeight
                    }
                  }
                }
              }
              thumbnail_subtitle {
                html
              }
              thumbnail_subtitle__mobile_ {
                html
              }
              thumbnail_format
            }
            uid
          }
        }
      }
    }
  `)

  useEffect(() => {
    setTimeout(() => {
      var msnry = new Masonry(".grid", {
        itemSelector: ".grid-item",
      })
    }, 100)
  }, [])

  return (
    <Grid className="grid">
      {data.allPrismicStory.edges.map(item => renderGridItem(item))}
    </Grid>
  )
}

export default StoriesGrid
