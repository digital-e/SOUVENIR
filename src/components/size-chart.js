import React, { useState, useContext, useEffect, useRef } from "react"

import { graphql, useStaticQuery } from "gatsby"

import styled from "styled-components"

const Text = styled.div`
  font-family: "Arial Narrow";
  font-size: 0.8rem;
  margin-top: 2rem;
  cursor: pointer;
  text-decoration: underline;
  color: black;
  width: fit-content;

  :hover {
    text-decoration: underline;
  }

  a {
    color: black;
  }
`

const PopupScreen = styled.div`
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 999;
  height: auto;
  width: auto;
  max-width: 100vw;
  min-width: 95vw;
  cursor: pointer;

  img {
    height: 100%;
    width: 100%;
  }

  @media (min-width: 576px) {
    min-width: auto;
  }
`

const PopupScreenBackground = styled.div`
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 999;
  height: 100vh;
  width: 100vw;
  cursor: pointer;
  background-color: rgba(0, 0, 0, 0.3);
`

const SizeChart = props => {
  let [showPopup, setShowPopup] = useState(false)
  let query = useStaticQuery(graphql`
    {
      allPrismicSizecharts {
        edges {
          node {
            data {
              size_charts {
                size_chart_image {
                  url
                }
                size_chart_tag
              }
            }
          }
        }
      }
    }
  `)

  const togglePopup = () => {
    setShowPopup(!showPopup)
  }

  let data = query.allPrismicSizecharts.edges[0].node.data.size_charts
  let tag = null

  props.product.tags.length === 1 && (tag = props.product.tags[0])

  let chart = data.find(item => {
    return item.size_chart_tag === tag
  })

  return (
    <>
      {tag === null ? null : (
        <>
          <Text onClick={togglePopup}>SIZE CHART</Text>
          {showPopup ? (
            <>
              <PopupScreenBackground onClick={togglePopup} />
              <PopupScreen onClick={togglePopup}>
                <img src={chart.size_chart_image.url} />
              </PopupScreen>
            </>
          ) : null}
        </>
      )}
    </>
  )
}

export default SizeChart
