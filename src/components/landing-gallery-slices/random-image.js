import React from "react"
import styled from "styled-components"

import Img from "gatsby-image"

const StyledImg = styled(Img)`
  position: absolute !important;
  z-index: 997;
  visibility: hidden;
  height: 100vh;
  width: 100vw;
  background-color: white;
`

class RandomImage extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {}

    this.interval = null
  }

  componentDidMount() {
    let index = 0
    let length = this.props.data.items.length

    let random = Math.floor(Math.random() * length)

    let images = document.querySelectorAll(".intro-img")

    images[index].classList.add("top")

    images.forEach(item => {
      item.classList.remove("top")
    })

    images[random].classList.add("top")
  }

  render() {
    return (
      <>
        {this.props.data.items.map((item, index) => (
          <StyledImg
            className="intro-img"
            key={index}
            fluid={item.image.fluid}
          />
        ))}
      </>
    )
  }
}

export default RandomImage
