import React from "react"
import styled from "styled-components"

import Img from "gatsby-image"

const StyledImg = styled(Img)`
  position: absolute !important;
  z-index: 997;
  visibility: hidden;
  height: 100vh;
  width: 100vw;
`

class Slideshow extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {}

    this.interval = null
    this.intervalTime = this.props.data.primary.interval_duration
      ? this.props.data.primary.interval_duration
      : 4000
  }

  componentDidMount() {
    let index = 0
    let length = this.props.data.items.length

    this.containerRef.childNodes[index].classList.add("top")
    this.interval = setInterval(() => {
      this.containerRef.childNodes.forEach(item => {
        item.classList.remove("top")
      })
      this.containerRef.childNodes[index].classList.add("top")
      index++
      if (index > length - 1) index = 0
    }, this.intervalTime)
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  render() {
    return (
      <div ref={el => (this.containerRef = el)}>
        {this.props.data.items.map((item, index) => (
          <StyledImg
            className="intro-img"
            key={index}
            fluid={item.image.fluid}
          />
        ))}
      </div>
    )
  }
}

export default Slideshow
