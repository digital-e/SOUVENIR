import React from "react"
import Plyr from "plyr"

import Player from "../videoplayer"

class Video extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = { players: null }
  }

  componentDidMount() {
    this.setState(
      {
        players: Array.from(document.querySelectorAll("#player")).map(
          p =>
            new Plyr(p, {
              displayDuration: false,
              controls: false,
              muted: true,
            })
        ),
      },
      () => {
        if (this.state.players === null) return
        setTimeout(() => {
          this.state.players.forEach(player => {
            player.volume = 0
            player.play()
          })
        }, 1000)
      }
    )
  }

  componentWillUnmount() {}

  render() {
    return this.props.data.primary.vimeo_link_id !== null ? (
      <Player
        player={this.state.players !== null ? this.state.players[0] : null}
        embed={`<iframe src="https://player.vimeo.com/video/${this.props.data.primary.vimeo_link_id}?color=ffffff&title=false&byline=false&loop=true&portrait=0&quality=240p" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>`}
      />
    ) : null
  }
}

export default Video
