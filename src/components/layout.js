/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"

import Menu from "./menu"
// import StoreContext, { defaultStoreContext } from "../context/StoreContext"
import posed from "react-pose"

import "./layout.css"

const Transition = posed.div({
  preEnter: {
    delay: 0,
    y: 0,
    opacity: 0,
  },
  enter: {
    opacity: 1,
    y: 0,
    delay: 0,
    transition: {
      duration: 0,
      ease: "easeInOut",
    },
    applyAtStart: { display: "block" },
  },
  exit: {
    y: 0,
    opacity: 0,
    transition: {
      duration: 0,
      ease: "easeInOut",
    },
    applyAtEnd: { display: "none" },
  },
})

class Layout extends React.Component {
  render() {
    const { children } = this.props
    return (
      <>
        <Transition
          style={{
            position: "absolute",
            top: 0,
            // overflow: "hidden",
            width: "100vw",
            backgroundColor: "white",
          }}
        >
          {children}
        </Transition>
      </>
    )
  }
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
