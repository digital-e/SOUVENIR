import React, { useContext } from "react"

import styled from "styled-components"

import StoreContext from "../../../context/StoreContext"

const Item = styled.div`
  padding-bottom: 2rem;

  text-align: right;

  :nth-child(1) {
    padding-top: 2rem;
  }
`

const Title = styled.div`
  font-family: "Arial Narrow";
  text-decoration: none;
  margin: 0;
  padding: 0;
  font-size: 0.8rem;
  text-align: right;
`

const Size = styled.div`
  font-family: "Arial Narrow";
  text-decoration: none;
  margin: 0;
  padding: 0;
  font-size: 0.8rem;
  text-align: right;
`

const Price = styled.div`
  font-family: "Arial Narrow";
  text-decoration: none;
  margin: 0;
  padding: 0;
  font-size: 0.8rem;
  text-align: right;
`

const Quantity = styled.div`
  font-family: "Arial Narrow";
  text-decoration: none;
  margin: 0;
  padding: 0;
  font-size: 0.8rem;
  text-align: right;
`

const StyledButton = styled.div`
  font-family: "Arial Narrow";
  display: block;
  border: 0;
  background: transparent;
  font-size: 0.8rem;
  padding-top: 0.5rem;
  outline: none;
  color: rgb(0, 0, 255);
  cursor: pointer;
  width: 100%;
  text-align: right;
`

const AdjustQuantityContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
`

const AdjustQuantity = styled.span`
  cursor: pointer;
  padding-left: 0.5rem;
`

const LineItem = props => {
  const { line_item } = props
  const { removeLineItem, updateLineItem, client, checkout } = useContext(
    StoreContext
  )
  const variantImage = line_item.variant.image ? (
    <img
      src={line_item.variant.image.src}
      alt={`${line_item.title} product shot`}
      height="60px"
    />
  ) : null

  const selectedOptions = line_item.variant.selectedOptions
    ? line_item.variant.selectedOptions.map(
        option => `${option.name}: ${option.value} `
      )
    : null

  const handleRemove = () => {
    removeLineItem(client, checkout.id, line_item.id)
  }

  const handleAddQuantity = () => {
    let newQuantity = line_item.quantity + 1
    updateLineItem(client, checkout.id, line_item.id, newQuantity)
  }

  const handleRemoveQuantity = () => {
    let newQuantity = line_item.quantity - 1
    updateLineItem(client, checkout.id, line_item.id, newQuantity)
  }

  return (
    <Item>
      {variantImage}
      <Title>
        {line_item.title}
        {`  `}
        {line_item.variant.title === !"Default Title"
          ? line_item.variant.title
          : ""}
      </Title>
      <Size>{selectedOptions}</Size>
      <Price>{`${line_item.variant.price} EUR`}</Price>
      <Quantity>{line_item.quantity}</Quantity>
      <AdjustQuantityContainer>
        <AdjustQuantity onClick={handleRemoveQuantity}>-</AdjustQuantity>
        <AdjustQuantity onClick={handleAddQuantity}>+</AdjustQuantity>
      </AdjustQuantityContainer>
      <StyledButton onClick={handleRemove}>REMOVE</StyledButton>
    </Item>
  )
}

export default LineItem
