import React from "react"
import styled from "styled-components"

const Container = styled.div`
  margin: 4rem 1rem;

  word-break: break-word;

  @media (min-width: 992px) {
    margin: 4rem 2rem;
  }

  iframe {
    display: block;
    margin: 0 auto;
    max-width: 100%;
  }

  img {
    max-width: 100%;
  }
`

const Text = styled.div`
  font-family: "Arial Narrow";
  font-size: 1rem;
  width: 100%;
  margin: 0 auto;
  text-align: ${props => props.regularalign};

  @media (min-width: 992px) {
    font-size: 2rem;
    width: ${props =>
      props.centred === "Centred With Margins" ? "40%" : "100%"};
    margin: 0 auto;
  }

  .block-img {
    text-align: center;
  }
`

export default ({ ...props }) => {
  let { data } = props

  return (
    <Container>
      <Text
        dangerouslySetInnerHTML={{ __html: data.text_field.html }}
        centred={data.alignment}
        regularalign={data.regular_text_align}
      />
    </Container>
  )
}
