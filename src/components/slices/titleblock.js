import React from "react"
import styled from "styled-components"

const Container = styled.div`
  margin: 0 1rem;

  word-break: break-word;

  @media (min-width: 992px) {
    margin: 0 2rem;
  }
`

const Title = styled.div`
  font-family: "Arial Narrow Bold";
  text-align: center;
  font-size: 5rem;
  line-height: 5rem;
  margin-bottom: 2rem;

  @media (min-width: 992px) {
    text-align: center;
    font-size: 10rem;
    line-height: 10rem;
    margin-bottom: 0;
  }
`

const Text = styled.div`
  font-size: 1.5rem;
  margin-bottom: 2rem;
`

const Credits = styled.div`
  font-size: 0.8rem;
  font-family: courier;
  margin-bottom: 2rem;
`

export default ({ ...props }) => {
  let { data } = props

  return (
    <Container>
      <Title>{data.title.text}</Title>
      <Text dangerouslySetInnerHTML={{ __html: data.text.html }} />
      <Credits dangerouslySetInnerHTML={{ __html: data.credits.html }} />
    </Container>
  )
}
