import React from "react"
import styled from "styled-components"

import Img from "gatsby-image"

import { Image } from "../image"

const ImgContainer = styled.div`
  flex-basis: 100%;

  :nth-child(2) {
    margin: 0;
  }

  :first-child {
    margin: 0;
  }

  :last-child {
    margin: 0;
  }

  @media (min-width: 992px) {
    flex-basis: 33.3%;

    :nth-child(2) {
      margin: 0 1rem;
    }

    :first-child {
      margin-left: 2rem;
      margin-right: 1rem;
    }

    :last-child {
      margin-left: 1rem;
      margin-right: 2rem;
    }
  }
`

const StyledImg = styled(Img)``

const Credits = styled.div`
  text-align: center;
  padding: 0.5rem 0;

  strong {
    font-weight: bold !important;
  }
`

const Container = styled.div``

const Grid = styled.div`
  display: flex;
  flex-direction: column;

  @media (min-width: 992px) {
    flex-direction: row;
  }
`

export default ({ ...props }) => {
  let { data } = props

  return (
    <Container>
      <Grid>
        {Object.values(data).map((item, index) => {
          // if (index > 2) return
          return (
            <ImgContainer>
              {/* {item.localFile ? (
                <StyledImg fluid={item.localFile.childImageSharp.fluid} />
              ) : (
                <img src={item.src} />
              )} */}
              <Image src={item.url} fluid={item.localFile} />
              <Credits
                dangerouslySetInnerHTML={{
                  __html: item.alt,
                }}
              />
              {/* <Credits
                dangerouslySetInnerHTML={{
                  __html: Object.values(data)[index + 3].html,
                }}
              /> */}
            </ImgContainer>
          )
        })}
      </Grid>
    </Container>
  )
}
