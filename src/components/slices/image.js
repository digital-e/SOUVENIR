import React from "react"
import styled from "styled-components"

import Img from "gatsby-image"

import { Image } from "../image"

const StyledImg = styled(Img)``

const Credits = styled.div`
  text-align: ${props =>
    props.alignment === "Left"
      ? "left"
      : props.alignment === "Center"
      ? "center"
      : "right"};
  padding: 0.5rem;

  font-size: 1rem;

  strong {
    font-weight: bold !important;
  }
`

const Container = styled.div`
  margin: 0;

  @media (min-width: 992px) {
    margin: ${props => (props.alignment === "Full Width" ? 0 : "0 10rem")};
  }
`

export default ({ ...props }) => {
  let { data } = props

  return (
    <Container alignment={data.image_alignment}>
      {/* {data.image.localFile ? (
        <StyledImg fluid={data.image.localFile.childImageSharp.fluid} />
      ) : (
        <img src={data.image.src} />
      )} */}
      <Image src={data.image.url} fluid={data.image.localFile} />
      <Credits
        dangerouslySetInnerHTML={{ __html: data.credits.html }}
        alignment={data.credits_alignment}
      />
    </Container>
  )
}
