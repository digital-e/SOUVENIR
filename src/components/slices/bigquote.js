import React from "react"
import styled from "styled-components"

const Quote = styled.div`
  font-family: "Arial Narrow Bold";
  text-align: center;
  font-size: 3rem !important;
  width: 100%;
  line-height: 3rem !important;
  margin: 0 auto;
  word-break: break-word;

  h1 {
    font-size: 3rem !important;
  }

  @media (min-width: 992px) {
    text-align: center;
    font-size: 6rem !important;
    width: ${props =>
      props.centred === "Centred With Margins" ? "40%" : "100%"};
    line-height: 6rem !important;
    margin: 0 auto;

    h1 {
      font-size: 6rem !important;
    }
  }
`

const Container = styled.div`
  margin: 6rem 1rem;

  @media (min-width: 992px) {
    margin: 6rem 2rem;
  }
`

export default ({ ...props }) => {
  let { data } = props
  return (
    <Container>
      <Quote
        dangerouslySetInnerHTML={{ __html: data.quote.html }}
        centred={props.alignment}
      />
    </Container>
  )
}
