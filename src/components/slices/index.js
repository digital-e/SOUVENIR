import TitleBlock from "./titleblock"
import TitleBlockWithImage from "./titleblockwithimage"
import Image from "./image"
import Text from "./text"
import ImageGalleryX2 from "./image_gallery_x_2"
import ImageGalleryX3 from "./image_gallery_x_3"
import BigQuote from "./bigquote"
import Video from "./video"

export {
  TitleBlock,
  TitleBlockWithImage,
  Image,
  Text,
  ImageGalleryX2,
  ImageGalleryX3,
  BigQuote,
  Video,
}
