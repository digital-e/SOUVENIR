import React from "react"
import styled from "styled-components"

import Img from "gatsby-image"

import { Image } from "../image"

const Container = styled.div`
  margin: 0 1rem;
  display: flex;
  flex-direction: column;
  word-break: break-word;

  @media (min-width: 992px) {
    margin: 0 2rem;
    display: flex;
    flex-direction: row;
  }
`
const Information = styled.div`
  flex-basis: 50%;
  padding: 2rem 0rem;

  @media (min-width: 992px) {
    flex-basis: 50%;
    padding: 2rem 3rem;
  }
`

const ImageContainer = styled.div`
  flex-basis: 50%;
`

const Title = styled.div`
  font-family: "Arial Narrow";
  text-align: center;
  font-size: 5rem;
  line-height: 5rem;
  padding-bottom: 2rem;

  @media (min-width: 992px) {
    text-align: center;
    font-size: 10rem;
    line-height: 10rem;
    padding-bottom: 2rem;
  }
`

const Text = styled.div`
  font-size: 1.5rem;
  padding-bottom: 2rem;
`

const Credits = styled.div`
  font-size: 0.8rem;
  font-family: courier;
`

export default ({ ...props }) => {
  let { data } = props

  return (
    <Container>
      <ImageContainer>
        {/* {data.image.localFile ? (
          <Img fluid={data.image.localFile.childImageSharp.fluid} />
        ) : (
          <img src={data.image.src} />
        )} */}
        <Image src={data.image.url} fluid={data.image.localFile} />
      </ImageContainer>
      <Information>
        <Title>{data.title.text}</Title>
        <Text dangerouslySetInnerHTML={{ __html: data.text.html }} />
        <Credits dangerouslySetInnerHTML={{ __html: data.credits.html }} />
      </Information>
    </Container>
  )
}
