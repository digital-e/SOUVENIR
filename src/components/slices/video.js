import React from "react"
import styled from "styled-components"

const Container = styled.div`
  margin: 4rem 0rem;

  word-break: break-word;

  @media (min-width: 992px) {
    margin: 4rem 0rem;
  }
`

const Text = styled.div`
  width: 100%;
  margin: 0 auto;
`

export default ({ ...props }) => {
  let { data } = props

  return (
    <Container>
      <Text
        className="embed-container"
        dangerouslySetInnerHTML={{ __html: data.text_field.html }}
      />
    </Container>
  )
}
