import React from "react"

import styled from "styled-components"

import posed from "react-pose"

import { Controller, Scene } from "react-scrollmagic"

import SizeChart from "./size-chart"

import ProductForm from "./productForm"

const ProductBuyContainer = styled.div`
  width: 50%;
  height: 100%;
  font-size: 1rem;
  line-height: 1.5rem;

  // .shipping-dropdown {
  //   margin-bottom: 2rem;
  // }

  // .shipping-dropdown-text {
  //   padding-top: 1rem !important;
  // }

  @media (min-width: 576px) {
    margin-top: 10rem;
    padding-left: 5rem;
  }
`

const Text = styled.div`
  font-family: "Arial Narrow";
  margin-top: 3rem;
`

const preDropdownText = posed.div({
  open: {
    y: 0,
    opacity: 1,
    transition: {
      ease: "easeInOut",
    },
  },
  closed: {
    opacity: 0,
    transition: {
      ease: "easeInOut",
    },
  },
})

const DropdownText = styled(preDropdownText)`
  font-family: "Arial Narrow";
`

const preExpand = posed.div({
  open: {
    delayChildren: 50,
    staggerChildren: 10,
    maxHeight: 1000,
    applyAtStart: { display: "block" },
    transition: {
      ease: "easeInOut",
    },
  },
  closed: {
    maxHeight: 0,
    applyAtEnd: { display: "none" },
    transition: {
      ease: "easeInOut",
    },
  },
})

const EditorsNote = styled(preExpand)`
  font-family: "Arial Narrow";
  cursor: pointer;
  padding-bottom: 2rem;
`
const SizeAndFit = styled(preExpand)`
  font-family: "Arial Narrow";
  cursor: pointer;
  padding-bottom: 2rem;
`

const Shipping = styled(preExpand)`
  font-family: "Arial Narrow";
  cursor: pointer;
  padding-bottom: 2rem;
`
const DropdownTitle = styled.div`
  font-family: "Arial Narrow";
  cursor: pointer;
  display: flex;
  justify-content: space-between;
  margin-bottom: 1rem;
`

class ProductBuyContainerComponent extends React.PureComponent {
  //   static contextType = StoreContext
  constructor(props) {
    super(props)
    this.state = {
      isOpenEditorsNote: true,
      isOpenSizeAndFit: false,
      isOpenShipping: false,
      productPanelRightHeight: 0,
      productBuyContainerheight: 0,
      isMobile: false,
    }

    this.duration = 0

    this.toggleDropdownOne = this.toggleDropdownOne.bind(this)
    this.toggleDropdownTwo = this.toggleDropdownTwo.bind(this)
    this.toggleDropdownThree = this.toggleDropdownThree.bind(this)
  }

  componentDidMount() {
    if (window.innerWidth < 576) {
      this.setState({
        isMobile: true,
      })
    }

    setTimeout(() => {
      this.setState({
        // productPanelRightHeight: this.productPanelRightRef.getBoundingClientRect()
        //   .height,
        productBuyContainerHeight: this.productBuyContainerRef
          ? this.productBuyContainerRef.getBoundingClientRect().height
          : 0,
      })
    }, 0)
  }

  toggleDropdownOne() {
    this.setState(prevState => ({
      isOpenEditorsNote: !prevState.isOpenEditorsNote,
    }))
  }

  toggleDropdownTwo() {
    this.setState(prevState => ({
      isOpenSizeAndFit: !prevState.isOpenSizeAndFit,
    }))
  }

  toggleDropdownThree() {
    this.setState(prevState => ({
      isOpenShipping: !prevState.isOpenShipping,
    }))
  }

  render() {
    this.duration =
      this.props.productPanelRightHeight -
      this.state.productBuyContainerHeight -
      100

    let res = this.props.product.descriptionHtml
    let splitRes1 = res.match(/<div\sid="editors-note\s*?.*?">(.*?)<\/div>/gs)

    let splitRes2 = res.match(/<div\sid="size-and-fit\s*?.*?">(.*?)<\/div>/gs)

    splitRes1 !== null ? (splitRes1 = splitRes1[0]) : (splitRes1 = "")
    splitRes2 !== null ? (splitRes2 = splitRes2[0]) : (splitRes2 = "")

    return (
      <>
        {/* <Controller>
          <Scene
            enabled={this.state.isMobile ? false : true}
            duration={this.duration && this.duration > 0 ? this.duration : 0}
            offset={-150}
            triggerHook="onLeave"
            pin
          > */}
        <ProductBuyContainer>
          <div ref={el => (this.productBuyContainerRef = el)}>
            <ProductForm product={this.props.product} />
            <SizeChart product={this.props.product} />
            {/* <Text
              dangerouslySetInnerHTML={{
                __html: this.props.product.descriptionHtml,
              }}
            /> */}
            <div style={{ marginTop: "2rem" }}>
              <DropdownTitle onClick={this.toggleDropdownOne}>
                INFO <span>{this.state.isOpenEditorsNote ? "-" : "+"}</span>
              </DropdownTitle>
              <EditorsNote
                pose={this.state.isOpenEditorsNote ? "open" : "closed"}
              >
                <DropdownText
                  dangerouslySetInnerHTML={{
                    __html: splitRes1,
                  }}
                />
              </EditorsNote>
              <DropdownTitle onClick={this.toggleDropdownTwo}>
                SIZE & FIT{" "}
                <span>{this.state.isOpenSizeAndFit ? "-" : "+"}</span>
              </DropdownTitle>
              <SizeAndFit
                pose={this.state.isOpenSizeAndFit ? "open" : "closed"}
              >
                <DropdownText
                  dangerouslySetInnerHTML={{
                    __html: splitRes2,
                  }}
                />
              </SizeAndFit>
              <DropdownTitle
                onClick={this.toggleDropdownThree}
                className="shipping-dropdown"
              >
                SHIPPING
                <span>{this.state.isOpenShipping ? "-" : "+"}</span>
              </DropdownTitle>
              <Shipping
                pose={this.state.isOpenShipping ? "open" : "closed"}
                style={{ paddingBottom: "2rem" }}
              >
                <DropdownText
                  className="shipping-dropdown-text"
                  dangerouslySetInnerHTML={{
                    __html: this.props.shippingInfo,
                  }}
                />
              </Shipping>
            </div>
          </div>
        </ProductBuyContainer>
        {/* </Scene>
        </Controller> */}
      </>
    )
  }
}

export default ProductBuyContainerComponent
