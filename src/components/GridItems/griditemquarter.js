import React, { useState } from "react"

import { Link } from "gatsby"

import { Image } from "../image"

import styled from "styled-components"

const Container = styled.div`
  position: relative;
  float: left;
  width: 100%;
  height: 150vw;
  overflow: hidden;

  .imagecontainer-mobile {
    display: block;
  }

  .imagecontainer {
    display: none;
  }

  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }

  @media (min-width: 576px) {
    width: 25%;
    height: 32vw;

    .imagecontainer-mobile {
      display: none;
    }

    .imagecontainer {
      display: block;
    }
  }
`

const ImageContainer = styled.div`
  position: absolute;
  height: 100%;
  width: 100%;
`

const Information = styled.div`
  position: absolute;
  font-family: "Arial Narrow";
  font-size: 2rem;
  padding: 1rem;
  color: white;
  height: 100%;
  width: 100%;
  display: none;

  @media (min-width: 576px) {
    display: block;
  }
`

const BackdropFilter = styled.div`
  height: 100%;
  width: 100%;
  position: absolute;
  backdrop-filter: blur(20px);
  transition: opacity 0.3s;
`

const InformationMobile = styled.div`
  position: absolute;
  font-family: "Arial Narrow";
  font-size: 2rem;
  padding: 1rem;
  color: white;
  height: 100%;
  width: 100%;
  display: block;

  @media (min-width: 576px) {
    display: none;
  }
`

const GridItemQuarter = props => {
  const [isHovered, setIsHovered] = useState(false)

  return (
    <Link to={props.data.uid}>
      <Container
        onMouseEnter={() => setIsHovered(true)}
        onMouseLeave={() => setIsHovered(false)}
      >
        <ImageContainer className="image imagecontainer">
          {/* <img src={props.data.data.thumbnail.url} /> */}
          <Image fluid={props.data.data.thumbnail.localFile} />
        </ImageContainer>
        <ImageContainer className="image imagecontainer-mobile">
          {props.data.data.thumbnail_mobile.url !== null ? (
            // <img src={props.data.data.thumbnail_mobile.url} />
            <Image fluid={props.data.data.thumbnail_mobile.localFile} />
          ) : (
            // <img src={props.data.data.thumbnail.url} />
            <Image fluid={props.data.data.thumbnail.localFile} />
          )}
        </ImageContainer>
        <BackdropFilter
          style={isHovered ? { opacity: "1" } : { opacity: "0" }}
        />
        <Information
          // style={
          //   isHovered
          //     ? { backgroundColor: "black" }
          //     : { backgroundColor: "transparent" }
          // }
          dangerouslySetInnerHTML={{
            __html: props.data.data.thumbnail_subtitle.html,
          }}
        />
        <InformationMobile
          dangerouslySetInnerHTML={{
            __html: props.data.data.thumbnail_subtitle__mobile_.html,
          }}
        />
      </Container>
    </Link>
  )
}

export default GridItemQuarter
