import React, { useEffect, useRef } from "react"
import styled from "styled-components"

const Container = styled.div`
  // .plyr {
  //   z-index: -1;
  // }

  // .plyr__control {
  //   display: none !important;
  // }

  background: black;

  height: 100vh;

  display: flex;
  align-items: center;
  justify-content: center;

  .plyr--video .plyr__controls {
    padding: 0 !important;
  }

  .plyr__controls {
    width: 50% !important;
    left: 50% !important;
    bottom: 5% !important;
    transform: translateX(-50%) !important;
    background: none !important;
  }

  .plyr__control.plyr__tab-focus {
    box-shadow: none;
  }

  .plyr--video .plyr__control.plyr__tab-focus,
  .plyr--video .plyr__control:hover,
  .plyr--video .plyr__control[aria-expanded="true"] {
    background: none;
  }

  .plyr--full-ui input[type="range"] {
    color: white;
  }

  .plyr--airplay-supported [data-plyr="airplay"],
  .plyr--captions-enabled [data-plyr="captions"],
  .plyr--fullscreen-enabled [data-plyr="fullscreen"],
  .plyr--pip-supported [data-plyr="pip"] {
    display: none;
  }

  .plyr__tooltip {
    display: none;
  }

  .plyr__controls .plyr__controls__item.plyr__time {
    display: none;
  }

  .plyr__control[data-plyr="mute"] {
    // display: none !important;
  }

  input[type="range"]::-webkit-slider-thumb {
    opacity: 0;
  }

  input[type="range"]::-moz-range-thumb {
    opacity: 0;
  }

  input[type="range"]::-ms-thumb {
    opacity: 0;
  }

  .plyr__progress__buffer {
    border-radius: 0;
    display: none;
  }

  .plyr > button {
    display: none !important;
  }

  .plyr__volume input {
    display: none;
  }

  .plyr__volume {
    max-width: 35px !important;
  }

  .plyr {
    z-index: 0;
    width: 100% !important;
  }

  @media (min-width: 990px) {
    .plyr__video-wrapper {
      top: 50%;
      transform: translateY(-50%);
    }
  }

  // plyr__video-embed__container {
  //   transform: none !important;
  // }
`

const Video = styled.div``

export default props => {
  let videoRef = useRef()
  let playerResize = null

  useEffect(() => {
    playerResize = videoRef.current

    window.addEventListener("resize", resize)

    resize()
  }, [])

  const resize = () => {
    let ratio = 16 / 9
    let height = window.innerHeight
    let width = window.innerWidth
    let windowRatio = width / height

    if (windowRatio < ratio) {
      playerResize.style.transform = `scale(${ratio / windowRatio})`
    } else {
      playerResize.style.transform = `scale(1)`
    }
  }

  return (
    <Container>
      <Video
        className="plyr__video-embed"
        id="player"
        ref={videoRef}
        dangerouslySetInnerHTML={{
          __html: props.embed,
        }}
      />
    </Container>
  )
}
