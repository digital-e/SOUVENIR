import React from "react"

import styled from "styled-components"
import ProductThumbnail from "../components/product-thumbnail"

const ProductsGridContainer = styled.div`
  position: ${props => (props.isAbsolute ? "absolute" : "relative")};
  display: flex;
  width: 100vw;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
  box-sizing: border-box;
  background-color: white;
  top: 0;
  opacity: ${props => (props.isAbsolute ? 0 : 1)};
  z-index: ${props => (props.isAbsolute ? -1 : "")};

  div {
    flex-basis: 100%;
  }

  @media (min-width: 576px) {
    div {
      flex-basis: 50%;
    }
  }

  @media (min-width: 768px) {
    div {
      flex-basis: 33.3%;
    }
  }
`

class ProductsGrid extends React.PureComponent {
  render() {
    return (
      <ProductsGridContainer
        isAbsolute={this.props.isLanding}
        className={
          this.props.isLanding ? "landing-products-grid-container" : ""
        }
      >
        {this.props.allProducts.edges
          ? this.props.allProducts.edges.map((item, index) =>
              item.node.images.length >= 1 ? (
                <ProductThumbnail key={index} data={item} />
              ) : null
            )
          : this.props.allProducts.map((item, index) =>
              item.images.length >= 1 ? (
                <ProductThumbnail key={index} data={item} />
              ) : null
            )}
      </ProductsGridContainer>
    )
  }
}

export default ProductsGrid
