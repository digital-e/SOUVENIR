import React, { useContext } from "react"

import styled from "styled-components"

import StoreContext from "../../../context/StoreContext"

const Item = styled.div`
  padding-bottom: 2rem;

  text-align: center;

  img {
    padding: 1rem 0;
  }

  :nth-child(1) {
    padding-top: 2rem;
  }
`

const Title = styled.div`
  font-family: "Arial Narrow";
  text-decoration: none;
  margin: 0;
  padding: 0;
  font-size: 1rem;
  text-align: center;
`

const Size = styled.div`
  font-family: "Arial Narrow";
  text-decoration: none;
  margin: 0;
  padding: 0;
  font-size: 1rem;
  text-align: center;
`

const Quantity = styled.div`
  font-family: "Arial Narrow";
  text-decoration: none;
  margin: 0;
  padding: 0;
  font-size: 1rem;
  text-align: center;
`

const StyledButton = styled.div`
  font-family: "Arial Narrow";
  display: block;
  border: 0;
  background: transparent;
  font-size: 1rem;
  padding-top: 0.5rem;
  outline: none;
  color: rgb(0, 0, 255);
  cursor: pointer;
  width: 100%;
  text-align: center;
`

const AdjustQuantityContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`

const AdjustQuantity = styled.span`
  cursor: pointer;
  font-size: 1rem;
  padding: 0 0.5rem;
`

const LineItem = props => {
  const { line_item } = props
  const { removeLineItem, updateLineItem, client, checkout } = useContext(
    StoreContext
  )

  const variantImage = line_item.variant.image ? (
    <img
      src={line_item.variant.image.src}
      alt={`${line_item.title} product shot`}
      height="60px"
    />
  ) : null

  const selectedOptions = line_item.variant.selectedOptions
    ? line_item.variant.selectedOptions.map(
        option => `${option.name}: ${option.value} `
      )
    : null

  const handleRemove = () => {
    removeLineItem(client, checkout.id, line_item.id)
  }

  const handleAddQuantity = () => {
    let newQuantity = line_item.quantity + 1
    updateLineItem(client, checkout.id, line_item.id, newQuantity)
  }

  const handleRemoveQuantity = () => {
    let newQuantity = line_item.quantity - 1
    updateLineItem(client, checkout.id, line_item.id, newQuantity)
  }

  return (
    <Item>
      {variantImage}
      <Title>
        {line_item.title}
        {`  `}
        {line_item.variant.title === !"Default Title"
          ? line_item.variant.title
          : ""}
      </Title>
      <Size>
        {selectedOptions === !"Title: Default Title" ? selectedOptions : ""}
      </Size>
      <Quantity>{line_item.quantity}</Quantity>
      <AdjustQuantityContainer>
        <AdjustQuantity onClick={handleRemoveQuantity}>-</AdjustQuantity>
        <AdjustQuantity onClick={handleAddQuantity}>+</AdjustQuantity>
      </AdjustQuantityContainer>
      <StyledButton onClick={handleRemove}>REMOVE</StyledButton>
    </Item>
  )
}

export default LineItem
