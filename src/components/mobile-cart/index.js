import React, { useContext } from "react"

import StoreContext from "../../context/StoreContext"
import LineItem from "./LineItem"

import posed from "react-pose"

import styled from "styled-components"

const PosedCart = posed.div({
  open: {
    y: 0,
    opacity: 1,
    transition: {
      ease: "easeInOut",
    },
  },
  closed: {
    opacity: 0,
    transition: {
      ease: "easeInOut",
    },
  },
})

const Title = styled.div`
  font-family: "Arial Narrow";
  text-decoration: none;
  margin: 0;
  padding: 0;
  padding-top: 2rem;
  font-size: 1rem;
  text-align: center;
`

const Price = styled.div`
  font-family: "Arial Narrow";
  text-decoration: none;
  margin: 0;
  padding: 0;
  font-size: 1rem;
  text-align: center;
`

const CheckoutButton = styled.button`
  font-family: "Arial Narrow";
  display: block;
  border: 0;
  background: transparent;
  font-size: 1rem;
  padding-top: 0.5rem;
  outline: none;
  color: rgb(0, 0, 255);
  width: 100%;
  padding-bottom: 1rem;

  cursor: pointer;
`

class Cart extends React.Component {
  constructor(props) {
    super(props)

    this.context = null
  }

  componentDidMount() {
    this.context = this.context
  }

  handleCheckout = () => {
    if (this.props.quantityInCart === 0) return
    window.open(this.context.checkout.webUrl)
  }

  renderLineItems = () => {
    return this.context.checkout.lineItems.map(line_item => {
      return <LineItem key={line_item.id.toString()} line_item={line_item} />
    })
  }

  render() {
    return (
      <PosedCart
        posed={this.props.posed}
        style={{ paddingBottom: "7rem", paddingTop: "3rem" }}
      >
        {this.renderLineItems()}
        {/* <h2>Subtotal</h2>
        <p>{checkout.subtotalPrice} EUR</p>
        <br />
        <h2>Taxes</h2>
        <p>{checkout.totalTax} EUR</p>
        <br /> */}
        <Title>TOTAL</Title>
        <Price>{this.context.checkout.totalPrice} EUR</Price>
        <CheckoutButton onClick={this.handleCheckout}>
          {this.props.quantityInCart === 0 ? "EMPTY BAG" : "CHECK OUT"}
        </CheckoutButton>
      </PosedCart>
    )
  }
}

Cart.contextType = StoreContext

export default Cart
