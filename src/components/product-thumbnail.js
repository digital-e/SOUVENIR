import React from "react"
import Img from "gatsby-image"
import styled from "styled-components"
import { Link } from "gatsby"

const Container = styled.div`
  margin-bottom: 2rem;
  font-size: 0.8rem;
  color: black;

  a {
    text-decoration: none;
    color: black;
  }
`

const Title = styled.div`
  font-family: "Arial Narrow";
  color: black;
  width: fit-content;
  margin: 0 auto;
  padding-top: 15px;
`

const Price = styled.div`
  font-family: "Arial Narrow";
  color: black;
  width: fit-content;
  margin: 0 auto;
  padding-top: 5px;
`
const SoldOut = styled.span`
  font-family: "Arial Narrow";
  color: rgb(255, 55, 252);
  width: fit-content;
  margin: 0 auto;
  padding-left: 10px;
`

class ProductThumbnail extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      hover: false,
    }

    this.toggleHover = this.toggleHover.bind(this)
  }

  toggleHover() {
    if (window.innerWidth < 576) return
    this.setState(prevState => ({
      hover: !prevState.hover,
    }))
  }

  toggleHoverTouch() {
    this.setState(prevState => ({
      hover: !prevState.hover,
    }))
  }

  render() {
    return (
      <Container
        onMouseEnter={this.toggleHover}
        onMouseLeave={this.toggleHover}
        onTouchStart={() => this.toggleHoverTouch()}
      >
        {this.props.data.node ? (
          <Link to={`/products/${this.props.data.node.handle}`}>
            {this.props.data.node.images.length > 1 ? (
              <>
                <Img
                  fluid={
                    this.props.data.node.images[0].localFile.childImageSharp
                      .fluid
                  }
                  style={
                    this.state.hover
                      ? { display: "none" }
                      : { display: "block" }
                  }
                />
                <Img
                  fluid={
                    this.props.data.node.images[1].localFile.childImageSharp
                      .fluid
                  }
                  style={
                    this.state.hover
                      ? { display: "block" }
                      : { display: "none" }
                  }
                />
              </>
            ) : (
              <Img
                fluid={
                  this.props.data.node.images[0].localFile.childImageSharp.fluid
                }
                style={
                  this.state.hover ? { display: "block" } : { display: "block" }
                }
              />
            )}
            <Title>{this.props.data.node.title}</Title>
            <Price>
              {this.props.data.node.variants[0].price} EUR
              {!this.props.data.node.availableForSale ? (
                <SoldOut>SOLD OUT</SoldOut>
              ) : null}
            </Price>
          </Link>
        ) : (
          <Link to={`/products/${this.props.data.handle}`}>
            {this.props.data.images.length > 1 ? (
              <>
                <Img
                  fluid={
                    this.props.data.images[0].localFile.childImageSharp.fluid
                  }
                  style={
                    this.state.hover
                      ? { display: "none" }
                      : { display: "block" }
                  }
                />
                <Img
                  fluid={
                    this.props.data.images[1].localFile.childImageSharp.fluid
                  }
                  style={
                    this.state.hover
                      ? { display: "block" }
                      : { display: "none" }
                  }
                />
              </>
            ) : (
              <Img
                fluid={
                  this.props.data.images[0].localFile.childImageSharp.fluid
                }
                style={
                  this.state.hover ? { display: "block" } : { display: "block" }
                }
              />
            )}
            <Title>{this.props.data.title}</Title>
            <Price>
              {this.props.data.variants[0].price} EUR
              {!this.props.data.availableForSale ? (
                <SoldOut>SOLD OUT</SoldOut>
              ) : null}
            </Price>
          </Link>
        )}
      </Container>
    )
  }
}

export default ProductThumbnail
