import React, { useState, useEffect } from "react"
import { useStaticQuery, graphql } from "gatsby"

import styled from "styled-components"

const FixedBanner = styled.div`
  position: fixed;
  bottom: 0;
  z-index: 999;
  width: 100vw;
  height: auto;
  cursor: pointer;
  display: flex;
  justify-content: flex-end;

  font-family: "Arial Narrow";
  color: black;
  background-color: white;
  font-size: 0.9rem;
  padding: 0.3rem 1rem;
  box-sizing: border-box;
  line-height: 1;

  @media (min-width: 576px) {
    z-index: 997;
  }
`

const CookieBanner = ({ ...props }) => {
  const [toggleCookieBanner, setToggleCookieBanner] = useState(false)

  useEffect(() => {
    if (window.sessionStorage.getItem("cookieBannerHasShown")) return
    let cookieBanner = document.querySelector(".cookie-banner")
    setTimeout(() => {
      setToggleCookieBanner(true)
      cookieBanner.classList.remove("hide")
      cookieBanner.classList.add("show")
    }, 2000)

    //Check if Fixed Ticker has already shown
    window.sessionStorage.setItem("cookieBannerHasShown", true)
  }, [])

  const handleClick = () => {
    let cookieBanner = document.querySelector(".cookie-banner")
    setToggleCookieBanner(false)
    cookieBanner.classList.add("hide")
    cookieBanner.classList.remove("show")
    setTimeout(() => {
      cookieBanner.style.display = "none"
    }, 1000)
  }

  return (
    <FixedBanner onClick={handleClick} className={"cookie-banner hide"}>
      <span>
        To improve the user experience at souvenirofficial.com we use cookies.
        By continuing on the page you agree to our privacy policy.{" "}
        <u>I accept</u>
      </span>
      {/* <span className={"close"}>×</span> */}
    </FixedBanner>
  )
}

export default CookieBanner
